import React from "react";
import { ReactComponent as PDF } from "../../../../assets/svg/office/pdf.svg";
import astronomia from "../../../../assets/pdf/educationPage/260203/СВ 2021 гп/4. Рабочие программы/РП Астрономия СВ РУП 23 2021 гп.pdf";
import bj_sv_rup from "../../../../assets/pdf/educationPage/260203/СВ 2021 гп/4. Рабочие программы/РП БЖ СВ РУП 23 РУП 23 2021 гп.pdf";
import in_yaz from "../../../../assets/pdf/educationPage/260203/СВ 2021 гп/4. Рабочие программы/РП Ин.язык 26.02.03, РУП 23 2021 г.п..pdf";
import injenern_graf from "../../../../assets/pdf/educationPage/260203/СВ 2021 гп/4. Рабочие программы/РП Инженерная графика СВ РУП 23 2021 гп.pdf";
import inostr_v_prof from "../../../../assets/pdf/educationPage/260203/СВ 2021 гп/4. Рабочие программы/РП Иностранный в профессиональной деятельности 26.02.03 РУП 23 2021гп.pdf";
import in_1 from "../../../../assets/pdf/educationPage/260203/СВ 2021 гп/4. Рабочие программы/РП Иностранный язык 1 курс  26.02.03, РУП 23 2021 г.п..pdf";
import inform1 from "../../../../assets/pdf/educationPage/260203/СВ 2021 гп/4. Рабочие программы/РП Информатика 23.02.03.РУП 23 2021гп 252 СВ.pdf";
import inform2 from "../../../../assets/pdf/educationPage/260203/СВ 2021 гп/4. Рабочие программы/РП Информатика 23.02.03.РУП 23 2021гп.pdf";
import inform3 from "../../../../assets/pdf/educationPage/260203/СВ 2021 гп/4. Рабочие программы/РП Информатика 26.02.03, РУП 23 2021 СВ 1 курс.pdf";
import istor1 from "../../../../assets/pdf/educationPage/260203/СВ 2021 гп/4. Рабочие программы/РП История 1 курс 26.02.03, 2021 г.п..pdf";
import istor2 from "../../../../assets/pdf/educationPage/260203/СВ 2021 гп/4. Рабочие программы/РП История 2 курс 26.02.03, 2021 г.п..pdf";
import literatura from "../../../../assets/pdf/educationPage/260203/СВ 2021 гп/4. Рабочие программы/РП Литература 26.02.03, 2021 г.п..pdf";
import matem1 from "../../../../assets/pdf/educationPage/260203/СВ 2021 гп/4. Рабочие программы/РП Математика  26.02.03,РУП 23  2021 СВ 1 курс.pdf";
import matem2 from "../../../../assets/pdf/educationPage/260203/СВ 2021 гп/4. Рабочие программы/РП Математика 26.02.03  РУП 23 2021 гп.pdf";
import material from "../../../../assets/pdf/educationPage/260203/СВ 2021 гп/4. Рабочие программы/РП Материаловедение 26.02.03, РУП 23 2021 г.п..pdf";
import metrol from "../../../../assets/pdf/educationPage/260203/СВ 2021 гп/4. Рабочие программы/РП Метрология и стандартизация 26.02.03. РУП 23 2021 гп.pdf";
import mehanika from "../../../../assets/pdf/educationPage/260203/СВ 2021 гп/4. Рабочие программы/РП Механика 26.02.03. РУП 23 2021гп.pdf";
import obj from "../../../../assets/pdf/educationPage/260203/СВ 2021 гп/4. Рабочие программы/РП ОБЖ 26.02.03, РУП 23 2021 г.п..pdf";
import osnov_filos1 from "../../../../assets/pdf/educationPage/260203/СВ 2021 гп/4. Рабочие программы/РП Основы философии 26.02.03, 2021 г.п..pdf";
import osnov_fin from "../../../../assets/pdf/educationPage/260203/СВ 2021 гп/4. Рабочие программы/РП Основы финансовой грамотности 26.02.03, РУП 23 2021 г.п..pdf";
import pm1 from "../../../../assets/pdf/educationPage/260203/СВ 2021 гп/4. Рабочие программы/РП ПМ 1 26.02.03  543 СВ корр.pdf";
import pm12 from "../../../../assets/pdf/educationPage/260203/СВ 2021 гп/4. Рабочие программы/РП ПМ 1 26.02.03, РУП 23 2021 г.п..pdf";
import pm2 from "../../../../assets/pdf/educationPage/260203/СВ 2021 гп/4. Рабочие программы/РП ПМ 2 СВ 26.02.03, РУП 23 2021 г.п..pdf";
import pm3 from "../../../../assets/pdf/educationPage/260203/СВ 2021 гп/4. Рабочие программы/РП ПМ 3 СВ 26.02.03, 2021 г.п..pdf";
import pm4 from "../../../../assets/pdf/educationPage/260203/СВ 2021 гп/4. Рабочие программы/РП ПМ 4 26.02.03.РУП 23 2021 гп.pdf";
import pm5 from "../../../../assets/pdf/educationPage/260203/СВ 2021 гп/4. Рабочие программы/РП ПМ 5 26.02.03, 2021 г.п..pdf";
import popd from "../../../../assets/pdf/educationPage/260203/СВ 2021 гп/4. Рабочие программы/РП ПОПД 26.02.03, РУП 23 2021 г.п..pdf";
import psiholog from "../../../../assets/pdf/educationPage/260203/СВ 2021 гп/4. Рабочие программы/РП Психология общения 26.02.03, РУП 23  2021 г.п..pdf";
import rodnoy from "../../../../assets/pdf/educationPage/260203/СВ 2021 гп/4. Рабочие программы/РП Родной язык 26.02.03 РУП 23 2021 г.п..pdf";
import russkiy from "../../../../assets/pdf/educationPage/260203/СВ 2021 гп/4. Рабочие программы/РП Русский язык 26.02.03, РУП 23 2021 г.п..pdf";
import tus from "../../../../assets/pdf/educationPage/260203/СВ 2021 гп/4. Рабочие программы/РП ТУС 26.02.03, РУП 23  2021 г.п..pdf";
import fizika from "../../../../assets/pdf/educationPage/260203/СВ 2021 гп/4. Рабочие программы/РП Физика 26.02.03  2021 гп.pdf";
import fizra1 from "../../../../assets/pdf/educationPage/260203/СВ 2021 гп/4. Рабочие программы/РП Физкультура  26.02.03, РУП 23 2021 г.п..pdf";
import fizra2 from "../../../../assets/pdf/educationPage/260203/СВ 2021 гп/4. Рабочие программы/РП Физкультура 26.02.03,  РУП 23 2021 г.п.  2-5 курс.pdf";
import chimia from "../../../../assets/pdf/educationPage/260203/СВ 2021 гп/4. Рабочие программы/РП Химия 26.02.03, РУП 23 2021 г.п..pdf";
import seu from "../../../../assets/pdf/educationPage/260203/СВ 2021 гп/4. Рабочие программы/РП Экспл.СЭУ на ВУ 26.02.03, РУП 23 2021 г.п..pdf";
import electronica from "../../../../assets/pdf/educationPage/260203/СВ 2021 гп/4. Рабочие программы/РП Электроника и электротехника26.02.03. РУП 23  2021пг.pdf";
import rpeop from "../../../../assets/pdf/educationPage/260203/СВ 2021 гп/4. Рабочие программы/РП ЭОП 26.02.03,  РУП 23 2021 г.п..pdf";

const Navigation2021 = () => {
  return (
    <section className="workProgramm__wrapper">
      <h1>
        Основная профессиональная образовательная программа среднего
        профессионального образования для специальности 26.02.03 "Судовождение"
        (углубленная подготовка) (год набора 2021)
      </h1>
      <h2>Рабочие программы</h2>
      <table>
        <thead>
          <tr>
            <th>№ п/п</th>
            <th>Наименование дисциплины</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={astronomia}
                target={"_blank"}
                rel="noreferrer"
              >
                Астрономия СВ РУП 23 2021 гп
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>2</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={bj_sv_rup}
                target={"_blank"}
                rel="noreferrer"
              >
                БЖ СВ РУП 23 РУП 23 2021 гп
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>3</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={in_yaz}
                target={"_blank"}
                rel="noreferrer"
              >
                Ин.язык 26.02.03, РУП 23 2021 г.п
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>4</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={injenern_graf}
                target={"_blank"}
                rel="noreferrer"
              >
                Инженерная графика СВ РУП 23 2021 гп
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>5</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={inostr_v_prof}
                target={"_blank"}
                rel="noreferrer"
              >
                Иностранный в профессиональной деятельности 26.02.03 РУП 23
                2021гп
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>6</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={in_1}
                target={"_blank"}
                rel="noreferrer"
              >
                Иностранный язык 1 курс 26.02.03, РУП 23 2021 г.п.
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>7</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={inform1}
                target={"_blank"}
                rel="noreferrer"
              >
                Информатика 23.02.03.РУП 23 2021гп 252 СВ
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>8</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={inform2}
                target={"_blank"}
                rel="noreferrer"
              >
                Информатика 23.02.03.РУП 23 2021гп
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>9</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={inform3}
                target={"_blank"}
                rel="noreferrer"
              >
                Информатика 26.02.03, РУП 23 2021 СВ 1 курс
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>10</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={istor1}
                target={"_blank"}
                rel="noreferrer"
              >
                История 1 курс 26.02.03, 2021 г.п.
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>11</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={istor2}
                target={"_blank"}
                rel="noreferrer"
              >
                История 2 курс 26.02.03, 2021 г.п.
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>12</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={literatura}
                target={"_blank"}
                rel="noreferrer"
              >
                Литература 26.02.03, 2021 г.п.
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>13</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={matem1}
                target={"_blank"}
                rel="noreferrer"
              >
                Математика 26.02.03,РУП 23 2021 СВ 1 курс
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>14</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={matem2}
                target={"_blank"}
                rel="noreferrer"
              >
                Математика 26.02.03 РУП 23 2021 гп.
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>15</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={material}
                target={"_blank"}
                rel="noreferrer"
              >
                РП Материаловедение 26.02.03, РУП 23 2021 г.п.
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>16</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={metrol}
                target={"_blank"}
                rel="noreferrer"
              >
                {" "}
                Метрология и стандартизация 26.02.03. РУП 23 2021 гп.
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>17</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={mehanika}
                target={"_blank"}
                rel="noreferrer"
              >
                Механика 26.02.03. РУП 23 2021гп.
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>18</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={obj}
                target={"_blank"}
                rel="noreferrer"
              >
                ОБЖ 26.02.03, РУП 23 2021 г.п.
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>19</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={osnov_filos1}
                target={"_blank"}
                rel="noreferrer"
              >
                Основы философии 26.02.03, 2021 г.п.
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>20</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={osnov_fin}
                target={"_blank"}
                rel="noreferrer"
              >
                Основы финансовой грамотности 26.02.03, РУП 23 2021 г.п.
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>21</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={pm1}
                target={"_blank"}
                rel="noreferrer"
              >
                ПМ 1 26.02.03 543 СВ корр.
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>22</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={pm12}
                target={"_blank"}
                rel="noreferrer"
              >
                ПМ 1 26.02.03, РУП 23 2021 г.п.
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>23</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={pm2}
                target={"_blank"}
                rel="noreferrer"
              >
                ПМ 2 СВ 26.02.03, РУП 23 2021 г.п.
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>24</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={pm3}
                target={"_blank"}
                rel="noreferrer"
              >
                ПМ 3 СВ 26.02.03, 2021 г.п.
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>25</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={pm4}
                target={"_blank"}
                rel="noreferrer"
              >
                ПМ 4 26.02.03.РУП 23 2021 гп.
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>26</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={pm5}
                target={"_blank"}
                rel="noreferrer"
              >
                ПМ 5 26.02.03, 2021 г.п.
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>27</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={popd}
                target={"_blank"}
                rel="noreferrer"
              >
                ПОПД 26.02.03, РУП 23 2021 г.п.
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>28</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={psiholog}
                target={"_blank"}
                rel="noreferrer"
              >
                Психология общения 26.02.03, РУП 23 2021 г.п.
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>29</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={rodnoy}
                target={"_blank"}
                rel="noreferrer"
              >
                Родной язык 26.02.03 РУП 23 2021 г.п.
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>30</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={russkiy}
                target={"_blank"}
                rel="noreferrer"
              >
                Русский язык 26.02.03, РУП 23 2021 г.п.
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>31</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={tus}
                target={"_blank"}
                rel="noreferrer"
              >
                ТУС 26.02.03, РУП 23 2021 г.п.
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>32</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={fizika}
                target={"_blank"}
                rel="noreferrer"
              >
                Физика 26.02.03 2021 гп.
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>33</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={fizra1}
                target={"_blank"}
                rel="noreferrer"
              >
                Физкультура 26.02.03, РУП 23 2021 г.п.
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>34</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={fizra2}
                target={"_blank"}
                rel="noreferrer"
              >
                Физкультура 26.02.03, РУП 23 2021 г.п. 2-5 курс
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>35</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={chimia}
                target={"_blank"}
                rel="noreferrer"
              >
                Химия 26.02.03, РУП 23 2021 г.п.
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>36</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={seu}
                target={"_blank"}
                rel="noreferrer"
              >
                Экспл. СЭУ на ВУ 26.02.03, РУП 23 2021 г.п.
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>37</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={electronica}
                target={"_blank"}
                rel="noreferrer"
              >
                Электроника и электротехника26.02.03. РУП 23 2021пг.
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>38</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={rpeop}
                target={"_blank"}
                rel="noreferrer"
              >
                ЭОП 26.02.03, РУП 23 2021 г.п.
              </a>
            </td>
            <td></td>
          </tr>
        </tbody>
      </table>
    </section>
  );
};
export default Navigation2021;
