import React from "react";
import { ReactComponent as PDF } from "../../../../assets/svg/office/pdf.svg";
import astronomia from "../../../../assets/pdf/educationPage/260206/ЭМ 2022 гп/4. Рабочие программы/РП Астрономия.pdf";
import bj_sv_rup from "../../../../assets/pdf/educationPage/260206/ЭМ 2022 гп/4. Рабочие программы/РП Безопасность жизнедеятельности.pdf";
import in_yaz from "../../../../assets/pdf/educationPage/260206/ЭМ 2022 гп/4. Рабочие программы/РП Инженерная графика.pdf";
import injenern_graf from "../../../../assets/pdf/educationPage/260206/ЭМ 2022 гп/4. Рабочие программы/РП Иностранный язык  2 курс.pdf";
import inostr from "../../../../assets/pdf/educationPage/260206/ЭМ 2022 гп/4. Рабочие программы/РП Иностранный язык 1 курс.pdf";
import inform1 from "../../../../assets/pdf/educationPage/260206/ЭМ 2022 гп/4. Рабочие программы/РП Информатика   1 курс.pdf";
import inform2 from "../../../../assets/pdf/educationPage/260206/ЭМ 2022 гп/4. Рабочие программы/РП Информатика и информационные технологии в профессиональной деятельности.pdf";
import istor1 from "../../../../assets/pdf/educationPage/260206/ЭМ 2022 гп/4. Рабочие программы/РП История 1 курс.pdf";
import istor2 from "../../../../assets/pdf/educationPage/260206/ЭМ 2022 гп/4. Рабочие программы/РП История 2 курс.pdf";
import literatura from "../../../../assets/pdf/educationPage/260206/ЭМ 2022 гп/4. Рабочие программы/РП Литература.pdf";
import matem1 from "../../../../assets/pdf/educationPage/260206/ЭМ 2022 гп/4. Рабочие программы/РП Математика   2курс.pdf";
import matem2 from "../../../../assets/pdf/educationPage/260206/ЭМ 2022 гп/4. Рабочие программы/РП Математика 1 курс.pdf";
import material from "../../../../assets/pdf/educationPage/260206/ЭМ 2022 гп/4. Рабочие программы/РП Материаловедение.pdf";
import metrol from "../../../../assets/pdf/educationPage/260206/ЭМ 2022 гп/4. Рабочие программы/РП Метрология и стандартизация.pdf";
import mehanika from "../../../../assets/pdf/educationPage/260206/ЭМ 2022 гп/4. Рабочие программы/РП Механика.pdf";
import obj from "../../../../assets/pdf/educationPage/260206/ЭМ 2022 гп/4. Рабочие программы/РП ОБЖ.pdf";
import osnov_filos1 from "../../../../assets/pdf/educationPage/260206/ЭМ 2022 гп/4. Рабочие программы/РП Основы философии.pdf";
import osnov_fin from "../../../../assets/pdf/educationPage/260206/ЭМ 2022 гп/4. Рабочие программы/РП ПМ 2,.pdf";
import pm1 from "../../../../assets/pdf/educationPage/260206/ЭМ 2022 гп/4. Рабочие программы/РП ПМ 3.pdf";
import pm2 from "../../../../assets/pdf/educationPage/260206/ЭМ 2022 гп/4. Рабочие программы/РП ПМ 4.pdf";
import pm3 from "../../../../assets/pdf/educationPage/260206/ЭМ 2022 гп/4. Рабочие программы/РП ПМ1 прием  2022-2023копия.pdf";
import pm4 from "../../../../assets/pdf/educationPage/260206/ЭМ 2022 гп/4. Рабочие программы/РП Психология.pdf";
import psiholog from "../../../../assets/pdf/educationPage/260206/ЭМ 2022 гп/4. Рабочие программы/РП Родной язык.pdf";
import rodnoy from "../../../../assets/pdf/educationPage/260206/ЭМ 2022 гп/4. Рабочие программы/РП Русский язык.pdf";
import russkiy from "../../../../assets/pdf/educationPage/260206/ЭМ 2022 гп/4. Рабочие программы/РП Теория устройства судна.pdf";
import tus from "../../../../assets/pdf/educationPage/260206/ЭМ 2022 гп/4. Рабочие программы/РП Физика.pdf";
import toe from "../../../../assets/pdf/educationPage/260206/ЭМ 2022 гп/4. Рабочие программы/РП Физкультура   2-4.pdf";
import fizika from "../../../../assets/pdf/educationPage/260206/ЭМ 2022 гп/4. Рабочие программы/РП Физкультура.pdf";
import fizra1 from "../../../../assets/pdf/educationPage/260206/ЭМ 2022 гп/4. Рабочие программы/РП Химия.pdf";
import fizra2 from "../../../../assets/pdf/educationPage/260206/ЭМ 2022 гп/4. Рабочие программы/РП Экологические основы природопользования.pdf";
import chimia from "../../../../assets/pdf/educationPage/260206/ЭМ 2022 гп/4. Рабочие программы/РП Электрон и электротех..pdf";
import electr from "../../../../assets/pdf/educationPage/260206/ЭМ 2022 гп/4. Рабочие программы/РП ЭОП 26.02.06, 2021 г.п., РУП 19.pdf";
import rpeop from "../../../../assets/pdf/educationPage/260206/ЭМ 2022 гп/4. Рабочие программы/РП ЭОП 26.02.06, 2021 г.п., РУП 19.xlsx";

const MarineElectricalEquipment2022 = () => {
  return (
    <section className="workProgramm__wrapper">
      <h1>
        Основная профессиональная образовательная программа среднего
        профессионального образования для специальности 26.02.06 "Эксплуатация
        судового электрооборудования и средств автоматики" (год набора 2022)
      </h1>
      <h2>Рабочие программы</h2>
      <table>
        <thead>
          <tr>
            <th>№ п/п</th>
            <th>Наименование дисциплины</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={astronomia}
                target={"_blank"}
                rel="noreferrer"
              >
                Астрономия
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>2</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={bj_sv_rup}
                target={"_blank"}
                rel="noreferrer"
              >
                Безопасность жизнедеятельности
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>3</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={in_yaz}
                target={"_blank"}
                rel="noreferrer"
              >
                Инженерная графика
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>4</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={injenern_graf}
                target={"_blank"}
                rel="noreferrer"
              >
                Иностранный язык 2 курс
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>5</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={inostr}
                target={"_blank"}
                rel="noreferrer"
              >
                Иностранный язык 1 курс
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>6</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={inform1}
                target={"_blank"}
                rel="noreferrer"
              >
                Информатика 1 курс
              </a>
            </td>
            <td></td>
          </tr>

          <tr>
            <td>7</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={inform2}
                target={"_blank"}
                rel="noreferrer"
              >
                Информатика и информационные технологии в профессиональной
                деятельност
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>8</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={istor1}
                target={"_blank"}
                rel="noreferrer"
              >
                История 1 курс
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>9</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={istor2}
                target={"_blank"}
                rel="noreferrer"
              >
                История 2 курс
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>10</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={literatura}
                target={"_blank"}
                rel="noreferrer"
              >
                Литература
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>11</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={matem1}
                target={"_blank"}
                rel="noreferrer"
              >
                Математика 2 курс
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>12</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={matem2}
                target={"_blank"}
                rel="noreferrer"
              >
                Математика 1 курс
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>13</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={material}
                target={"_blank"}
                rel="noreferrer"
              >
                Материаловедение
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>14</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={metrol}
                target={"_blank"}
                rel="noreferrer"
              >
                {" "}
                Метрология и стандартизация
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>15</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={mehanika}
                target={"_blank"}
                rel="noreferrer"
              >
                Механика
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>16</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={obj}
                target={"_blank"}
                rel="noreferrer"
              >
                ОБЖ
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>17</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={osnov_filos1}
                target={"_blank"}
                rel="noreferrer"
              >
                Основы философии
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>18</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={osnov_fin}
                target={"_blank"}
                rel="noreferrer"
              >
                ПМ 2
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>19</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={pm1}
                target={"_blank"}
                rel="noreferrer"
              >
                ПМ 3
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>20</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={pm2}
                target={"_blank"}
                rel="noreferrer"
              >
                ПМ 4
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>21</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={pm3}
                target={"_blank"}
                rel="noreferrer"
              >
                ПМ1 прием 2022-2023
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>22</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={pm4}
                target={"_blank"}
                rel="noreferrer"
              >
                Психология
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>23</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={psiholog}
                target={"_blank"}
                rel="noreferrer"
              >
                Родной язык
              </a>
            </td>
            <td></td>
          </tr>

          <tr>
            <td>24</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={rodnoy}
                target={"_blank"}
                rel="noreferrer"
              >
                Русский язык
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>25</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={russkiy}
                target={"_blank"}
                rel="noreferrer"
              >
                Теория устройства судна
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>26</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={tus}
                target={"_blank"}
                rel="noreferrer"
              >
                Физика
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>27</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={toe}
                target={"_blank"}
                rel="noreferrer"
              >
                Физкультура 2-4
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>28</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={toe}
                target={"_blank"}
                rel="noreferrer"
              >
                Физкультура
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>29</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={fizika}
                target={"_blank"}
                rel="noreferrer"
              >
                Физкультура
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>30</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={fizra1}
                target={"_blank"}
                rel="noreferrer"
              >
                Химия
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>31</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={fizra2}
                target={"_blank"}
                rel="noreferrer"
              >
                Экологические основы природопользования
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>32</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={chimia}
                target={"_blank"}
                rel="noreferrer"
              >
                Электрон и электротех
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>33</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={electr}
                target={"_blank"}
                rel="noreferrer"
              >
                ЭОП 26.02.06, 2021 г.п., РУП 19
              </a>
            </td>
            <td></td>
          </tr>

          <tr>
            <td>34</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={rpeop}
                target={"_blank"}
                rel="noreferrer"
              >
                ЭОП 26.02.06, 2021 г.п., РУП 19
              </a>
            </td>
            <td></td>
          </tr>
        </tbody>
      </table>
    </section>
  );
};
export default MarineElectricalEquipment2022;
