import React from "react";
import { ReactComponent as PDF } from "../../../../assets/svg/office/pdf.svg";
import astronomia from "../../../../assets/pdf/educationPage/260205/СМ 2021 гп/4. Рабочие программы/РП Астрономия 26.02.05, 2021 г.п. РУП 11.pdf";
import bj_sv_rup from "../../../../assets/pdf/educationPage/260205/СМ 2021 гп/4. Рабочие программы/РП БЖ  26.02.05, 2021 г.п. РУП 11.pdf";
import gidravlika from "../../../../assets/pdf/educationPage/260205/СМ 2021 гп/4. Рабочие программы/РП Гидравлика 26.02.05, 2021 г.п. РУП 11.pdf";
import injenern_graf from "../../../../assets/pdf/educationPage/260205/СМ 2021 гп/4. Рабочие программы/РП Инженерная графика 26.02.05, 2021 г.п. РУП 11.pdf";
import inostr_v_prof from "../../../../assets/pdf/educationPage/260205/СМ 2021 гп/4. Рабочие программы/РП Иностранный язык 1 курс  26.02.05, 2021 г.п. РУП 11.pdf";
import in_1 from "../../../../assets/pdf/educationPage/260205/СМ 2021 гп/4. Рабочие программы/РП Иностранный язык в профессиональной деятельности  26.02.05, 2021 г.п. РУП 11.pdf";
import inform1 from "../../../../assets/pdf/educationPage/260205/СМ 2021 гп/4. Рабочие программы/РП Информатика  26.02.05, 2021 СМ 1 курс 11.pdf";
import inform2 from "../../../../assets/pdf/educationPage/260205/СМ 2021 гп/4. Рабочие программы/РП Информатика и информационные технологии в профессиональной деятельности 23.02.05. 2021гп РУП 11.pdf";
import istor1 from "../../../../assets/pdf/educationPage/260205/СМ 2021 гп/4. Рабочие программы/РП История 1 курс 26.02.05, 2021 г.п. РУП 11.pdf";
import istor2 from "../../../../assets/pdf/educationPage/260205/СМ 2021 гп/4. Рабочие программы/РП История 26.02.05, 2021 г.п. РУП 11.pdf";
import literatura from "../../../../assets/pdf/educationPage/260205/СМ 2021 гп/4. Рабочие программы/РП Литература 26.02.05, 2021 г.п. РУП 11.pdf";
import matem1 from "../../../../assets/pdf/educationPage/260205/СМ 2021 гп/4. Рабочие программы/РП Математика  26.02.05, 2021 СМ 1 курс 11.pdf";
import matem2 from "../../../../assets/pdf/educationPage/260205/СМ 2021 гп/4. Рабочие программы/РП Математика 26.02.05  2021 гп РУП 11.pdf";
import material from "../../../../assets/pdf/educationPage/260205/СМ 2021 гп/4. Рабочие программы/РП Материаловедение 26.02.05, 2021 г.п. РУП 11.pdf";
import metrol from "../../../../assets/pdf/educationPage/260205/СМ 2021 гп/4. Рабочие программы/РП Метрология и стандартизация 26.02.05, 2021 г.п. РУП 11.pdf";
import obj from "../../../../assets/pdf/educationPage/260205/СМ 2021 гп/4. Рабочие программы/РП ОБЖ 26.02.05, 2021 г.п. РУП 11.pdf";
import osnov_filos1 from "../../../../assets/pdf/educationPage/260205/СМ 2021 гп/4. Рабочие программы/РП Основы философии 26.02.05, 2021 г.п. РУП 11.pdf";
import osnov_fin from "../../../../assets/pdf/educationPage/260205/СМ 2021 гп/4. Рабочие программы/РП Основы финансовой грамотности 26.02.05, 2021 г.п. РУП 11.pdf";
import ohr_truda from "../../../../assets/pdf/educationPage/260205/СМ 2021 гп/4. Рабочие программы/РП Охрана труда 26.02.05, 2021 г.п., РУП 11.pdf";
import pm1 from "../../../../assets/pdf/educationPage/260205/СМ 2021 гп/4. Рабочие программы/РП ПМ 1, 26.02.05, 2021 г.п. РУП 11.pdf";
import pm2 from "../../../../assets/pdf/educationPage/260205/СМ 2021 гп/4. Рабочие программы/РП ПМ 2, 26.02.05, 2021 г.п. РУП 11.pdf";
import pm3 from "../../../../assets/pdf/educationPage/260205/СМ 2021 гп/4. Рабочие программы/РП ПМ 3, 26.02.05, 2021 г.п. РУП 11.pdf";
import pm4 from "../../../../assets/pdf/educationPage/260205/СМ 2021 гп/4. Рабочие программы/РП ПМ 4, 26.02.05, 2021 г.п. РУП 11.pdf";
import pm02 from "../../../../assets/pdf/educationPage/260205/СМ 2021 гп/4. Рабочие программы/РП ПМ.02 Обеспечение безопасности плавания 26.02.05 2021 г.п. РУП 11.pdf";
import psiholog from "../../../../assets/pdf/educationPage/260205/СМ 2021 гп/4. Рабочие программы/РП Психология общения 26.02.05, 2021 г.п. РУП 11.pdf";
import rodnoy from "../../../../assets/pdf/educationPage/260205/СМ 2021 гп/4. Рабочие программы/РП Родной язык 26.02.05 2021 г.п.РУП 11.pdf";
import russkiy from "../../../../assets/pdf/educationPage/260205/СМ 2021 гп/4. Рабочие программы/РП Русский язык 26.02.05, 2021 г.п. РУП 11.pdf";
import tus from "../../../../assets/pdf/educationPage/260205/СМ 2021 гп/4. Рабочие программы/РП ТУС 26.02.05, 2021  г.п. РУП 11.pdf";
import teh_meh from "../../../../assets/pdf/educationPage/260205/СМ 2021 гп/4. Рабочие программы/РП Техническая Механика 26.02.05, 2020 г.п. РУП 11.pdf";
import teh_termodin from "../../../../assets/pdf/educationPage/260205/СМ 2021 гп/4. Рабочие программы/РП Техническая термодинамика и теплопередача 26.02.05, 2021 г.п. РУП 11.pdf";
import fizika from "../../../../assets/pdf/educationPage/260205/СМ 2021 гп/4. Рабочие программы/РП Физика 26.02.05, 2021 г.п. РУП 11.pdf";
import fizra1 from "../../../../assets/pdf/educationPage/260205/СМ 2021 гп/4. Рабочие программы/РП Физкультура 1 курс 26.02.05, 2021 г.п. РУП 11.pdf";
import fizra2 from "../../../../assets/pdf/educationPage/260205/СМ 2021 гп/4. Рабочие программы/РП Физкультура 26.02.05, 2021  г.п. РУП 11.pdf";
import chimia from "../../../../assets/pdf/educationPage/260205/СМ 2021 гп/4. Рабочие программы/РП Химия 26.02.05, 2021 г.п. РУП 11.pdf";
import electronika from "../../../../assets/pdf/educationPage/260205/СМ 2021 гп/4. Рабочие программы/РП Электроника и электротехника 26.02.05, 2021 г.п. РУП 11.pdf";
import rpeop from "../../../../assets/pdf/educationPage/260205/СМ 2021 гп/4. Рабочие программы/РП ЭОП 26.02.05, 2021 г.п. РУП 11.pdf";

const ShipPowerPlants2021 = () => {
  return (
    <section className="workProgramm__wrapper">
      <h1>
        Основная профессиональная образовательная программа среднего
        профессионального образования для специальности 26.02.05 "Эксплуатация
        судовых энергетических установок" (год набора 2021)
      </h1>
      <h2>Рабочие программы</h2>
      <table>
        <thead>
          <tr>
            <th>№ п/п</th>
            <th>Наименование дисциплины</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={astronomia}
                target={"_blank"}
                rel="noreferrer"
              >
                РП Астрономия 26.02.05, 2021 г.п. РУП 11
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>2</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={bj_sv_rup}
                target={"_blank"}
                rel="noreferrer"
              >
                РП БЖ 26.02.05, 2021 г.п. РУП 11.
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>3</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={gidravlika}
                target={"_blank"}
                rel="noreferrer"
              >
                Гидравлика 26.02.05, 2021 г.п. РУП 11
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>4</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={injenern_graf}
                target={"_blank"}
                rel="noreferrer"
              >
                Инженерная графика 26.02.05, 2021 г.п. РУП 11
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>5</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={inostr_v_prof}
                target={"_blank"}
                rel="noreferrer"
              >
                Иностранный язык 1 курс 26.02.05, 2021 г.п. РУП 11
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>6</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={in_1}
                target={"_blank"}
                rel="noreferrer"
              >
                Иностранный язык в профессиональной деятельности 26.02.05, 2021
                г.п. РУП 11
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>7</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={inform1}
                target={"_blank"}
                rel="noreferrer"
              >
                РП Информатика 26.02.05, 2021 СМ 1 курс 11
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>8</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={inform2}
                target={"_blank"}
                rel="noreferrer"
              >
                РП Информатика и информационные технологии в профессиональной
                деятельности 23.02.05. 2021гп РУП 11
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>9</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={istor1}
                target={"_blank"}
                rel="noreferrer"
              >
                РП История 1 курс 26.02.05, 2021 г.п. РУП 11
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>10</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={istor2}
                target={"_blank"}
                rel="noreferrer"
              >
                РП История 26.02.05, 2021 г.п. РУП 11
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>11</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={literatura}
                target={"_blank"}
                rel="noreferrer"
              >
                РП Литература 26.02.05, 2021 г.п. РУП 11
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>12</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={matem1}
                target={"_blank"}
                rel="noreferrer"
              >
                РП Математика 26.02.05, 2021 СМ 1 курс 11
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>13</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={matem2}
                target={"_blank"}
                rel="noreferrer"
              >
                РП Математика 26.02.05 2021 гп РУП 11.
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>14</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={material}
                target={"_blank"}
                rel="noreferrer"
              >
                РП Материаловедение 26.02.05, 2021 г.п. РУП 11
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>15</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={metrol}
                target={"_blank"}
                rel="noreferrer"
              >
                РП Метрология и стандартизация 26.02.05, 2021 г.п. РУП 11
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>16</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={obj}
                target={"_blank"}
                rel="noreferrer"
              >
                {" "}
                РП ОБЖ 26.02.05, 2021 г.п. РУП 11
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>17</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={osnov_filos1}
                target={"_blank"}
                rel="noreferrer"
              >
                РП Основы философии 26.02.05, 2021 г.п. РУП 11
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>18</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={osnov_fin}
                target={"_blank"}
                rel="noreferrer"
              >
                РП Основы финансовой грамотности 26.02.05, 2021 г.п. РУП 11
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>19</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={ohr_truda}
                target={"_blank"}
                rel="noreferrer"
              >
                РП Охрана труда 26.02.05, 2021 г.п., РУП 11
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>20</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={pm1}
                target={"_blank"}
                rel="noreferrer"
              >
                РП ПМ 1, 26.02.05, 2021 г.п. РУП 11
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>21</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={pm2}
                target={"_blank"}
                rel="noreferrer"
              >
                РП ПМ 2, 26.02.05, 2021 г.п. РУП 11
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>22</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={pm3}
                target={"_blank"}
                rel="noreferrer"
              >
                РП ПМ 3, 26.02.05, 2021 г.п. РУП 11
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>23</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={pm4}
                target={"_blank"}
                rel="noreferrer"
              >
                РП ПМ 4, 26.02.05, 2021 г.п. РУП 11
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>24</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={pm02}
                target={"_blank"}
                rel="noreferrer"
              >
                РП ПМ.02 Обеспечение безопасности плавания 26.02.05 2021 г.п.
                РУП 11
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>25</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={psiholog}
                target={"_blank"}
                rel="noreferrer"
              >
                Психология общения 26.02.05, 2021 г.п. РУП 11.
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>26</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={rodnoy}
                target={"_blank"}
                rel="noreferrer"
              >
                РП Родной язык 26.02.05 2021 г.п.РУП 11
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>27</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={russkiy}
                target={"_blank"}
                rel="noreferrer"
              >
                РП Русский язык 26.02.05, 2021 г.п. РУП 11
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>28</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={tus}
                target={"_blank"}
                rel="noreferrer"
              >
                РП ТУС 26.02.05, 2021 г.п. РУП 11
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>29</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={teh_meh}
                target={"_blank"}
                rel="noreferrer"
              >
                РП Техническая Механика 26.02.05, 2020 г.п. РУП 11
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>30</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={teh_termodin}
                target={"_blank"}
                rel="noreferrer"
              >
                РП Техническая термодинамика и теплопередача 26.02.05, 2021 г.п.
                РУП 11
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>31</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={fizika}
                target={"_blank"}
                rel="noreferrer"
              >
                РП Физика 26.02.05, 2021 г.п. РУП 11
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>32</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={fizra1}
                target={"_blank"}
                rel="noreferrer"
              >
                РП Физкультура 1 курс 26.02.05, 2021 г.п. РУП 11
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>33</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={fizra2}
                target={"_blank"}
                rel="noreferrer"
              >
                РП Физкультура 26.02.05, 2021 г.п. РУП 11.
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>34</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={chimia}
                target={"_blank"}
                rel="noreferrer"
              >
                РП Химия 26.02.05, 2021 г.п. РУП 11.
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>35</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={electronika}
                target={"_blank"}
                rel="noreferrer"
              >
                РП Электроника и электротехника 26.02.05, 2021 г.п. РУП 11
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>36</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={rpeop}
                target={"_blank"}
                rel="noreferrer"
              >
                РП ЭОП 26.02.05, 2021 г.п. РУП 11
              </a>
            </td>
            <td></td>
          </tr>
        </tbody>
      </table>
    </section>
  );
};
export default ShipPowerPlants2021;
