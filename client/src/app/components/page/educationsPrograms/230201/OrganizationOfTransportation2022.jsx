import React from "react";
import { ReactComponent as PDF } from "../../../../assets/svg/office/pdf.svg";
import astronomia from "../../../../assets/pdf/educationPage/230201/ОП 2022 гп/4. Рабочие программы/РП  Астрономия 23.02.01, 2022 г.п., РУП 3.pdf";
import bj_sv_rup from "../../../../assets/pdf/educationPage/230201/ОП 2022 гп/4. Рабочие программы/РП  ПМ.01. 23.02.01, 2022 г.п., РУП 3.pdf";
import gidravlika from "../../../../assets/pdf/educationPage/230201/ОП 2022 гп/4. Рабочие программы/РП  ПМ.02. 23.02.01, 2022 г.п., РУП 3.pdf";
import injenern_graf from "../../../../assets/pdf/educationPage/230201/ОП 2022 гп/4. Рабочие программы/РП  ПМ.03. 23.02.01, 2022 г.п., РУП 3.pdf";
import inostr_v_prof from "../../../../assets/pdf/educationPage/230201/ОП 2022 гп/4. Рабочие программы/РП  ПМ.04. 23.02.01, 2022 г.п., РУП 3.pdf";
import in_1 from "../../../../assets/pdf/educationPage/230201/ОП 2022 гп/4. Рабочие программы/РП ВЧ.03. Коммерческая работа на транспорте 23.02.01, 2022 г.п., РУП 3.pdf";
import inform1 from "../../../../assets/pdf/educationPage/230201/ОП 2022 гп/4. Рабочие программы/РП ВЧ.04. Страхование и риски 23.02.01, 2022 г.п., РУП 3.pdf";
import inform2 from "../../../../assets/pdf/educationPage/230201/ОП 2022 гп/4. Рабочие программы/РП ВЧ.05. Экономика и управление на транспорте 23.02.01, 2022 г.п., РУП 3.pdf";
import istor1 from "../../../../assets/pdf/educationPage/230201/ОП 2022 гп/4. Рабочие программы/РП ВЧ.06. Теория и устройство судна 23.02.01, 2022 г.п., РУП 3.pdf";
import istor2 from "../../../../assets/pdf/educationPage/230201/ОП 2022 гп/4. Рабочие программы/РП ЕН.01. Математика 23.02.01, 2022 г.п., РУП 3.pdf";
import literatura from "../../../../assets/pdf/educationPage/230201/ОП 2022 гп/4. Рабочие программы/РП ЕН.02. Информатика 23.02.02, 2022 г.п., РУП 3.pdf";
import matem1 from "../../../../assets/pdf/educationPage/230201/ОП 2022 гп/4. Рабочие программы/РП Математика 1 курс 23.02.01, 2022 г.п., РУП 3.pdf";
import matem2 from "../../../../assets/pdf/educationPage/230201/ОП 2022 гп/4. Рабочие программы/РП Информатика 1 курс 23.02.01, 2022 г.п., РУП 32.pdf";
import metrol from "../../../../assets/pdf/educationPage/230201/ОП 2022 гп/4. Рабочие программы/РП ОБЖ 23.02.01, 2022 г.п., РУП 2.pdf";
import obj from "../../../../assets/pdf/educationPage/230201/ОП 2022 гп/4. Рабочие программы/РП ОГСЭ.04. Физическая культура 23.02.01, 2022 г.п., РУП 3.pdf";
import osnov_filos1 from "../../../../assets/pdf/educationPage/230201/ОП 2022 гп/4. Рабочие программы/РП ОП.01. Инженерная графика 23.02.01, 2022 г.п., РУП 3.pdf";
import osnov_fin from "../../../../assets/pdf/educationPage/230201/ОП 2022 гп/4. Рабочие программы/РП ОП.02. Электротехника и электроника 23.02.02, 2021 г.п., РУП 3.pdf";
import ohr_truda from "../../../../assets/pdf/educationPage/230201/ОП 2022 гп/4. Рабочие программы/РП ОП.03.Метрология, стандартизация и сертификация 23.02.01, 2022 г.п., РУП 3.pdf";
import pm1 from "../../../../assets/pdf/educationPage/230201/ОП 2022 гп/4. Рабочие программы/РП ОП.04 Транспортная система России 23.02.01, 2022 г.п., РУП 3.pdf";
import pm2 from "../../../../assets/pdf/educationPage/230201/ОП 2022 гп/4. Рабочие программы/РП ОП.05. Технические средства (по видам транспорта) 23.02.01, 2022 г.п., РУП 3.pdf";
import pm3 from "../../../../assets/pdf/educationPage/230201/ОП 2022 гп/4. Рабочие программы/РП ОП.06. ПОПД 23.02.01, 2022 г.п., РУП 3.pdf";
import pm4 from "../../../../assets/pdf/educationPage/230201/ОП 2022 гп/4. Рабочие программы/РП ОП.07. Охрана труда 23.02.01, 2022 г.п., РУП 3.pdf";
import pm02 from "../../../../assets/pdf/educationPage/230201/ОП 2022 гп/4. Рабочие программы/РП ОП.08. Безопасность жизнедеятельности 23.02.01, 2022 г.п., РУП 3.pdf";
import psiholog from "../../../../assets/pdf/educationPage/230201/ОП 2022 гп/4. Рабочие программы/РП Охрана труда 23.02.01 2022 г.п..pdf";
import rodnoy from "../../../../assets/pdf/educationPage/230201/ОП 2022 гп/4. Рабочие программы/РП Физика 23.02.01, 2022 г.п., РУП 3.pdf";
import russkiy from "../../../../assets/pdf/educationPage/230201/ОП 2022 гп/4. Рабочие программы/РП Физкультура 1 курс 23.02.01, 2022 г.п., РУП 3.pdf";
import tus from "../../../../assets/pdf/educationPage/230201/ОП 2022 гп/4. Рабочие программы/РП Химия 23.02.01, 2022 г.п., РУП 3.pdf";

const OrganizationOfTransportation2022 = () => {
  return (
    <section className="workProgramm__wrapper">
      <h1>
        Основная профессиональная образовательная программа среднего
        профессионального образования для специальности 23.02.01 "Организация
        перевозок и управление на транспорте" (год набора 2022)
      </h1>
      <h2>Рабочие программы</h2>
      <table>
        <thead>
          <tr>
            <th>№ п/п</th>
            <th>Наименование дисциплины</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={astronomia}
                target={"_blank"}
                rel="noreferrer"
              >
                РП Астрономия 23.02.01, 2022 г.п., РУП 3
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>2</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={bj_sv_rup}
                target={"_blank"}
                rel="noreferrer"
              >
                РП ПМ.01. 23.02.01, 2022 г.п., РУП 3
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>3</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={gidravlika}
                target={"_blank"}
                rel="noreferrer"
              >
                РП ПМ.02. 23.02.01, 2022 г.п., РУП 3
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>4</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={injenern_graf}
                target={"_blank"}
                rel="noreferrer"
              >
                РП ПМ.03. 23.02.01, 2022 г.п., РУП 3
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>5</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={inostr_v_prof}
                target={"_blank"}
                rel="noreferrer"
              >
                РП ПМ.04. 23.02.01, 2022 г.п., РУП 3
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>6</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={in_1}
                target={"_blank"}
                rel="noreferrer"
              >
                РП ВЧ.03. Коммерческая работа на транспорте 23.02.01, 2022 г.п.,
                РУП 3
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>7</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={inform1}
                target={"_blank"}
                rel="noreferrer"
              >
                РП ВЧ.04. Страхование и риски 23.02.01, 2022 г.п., РУП 3
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>8</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={inform2}
                target={"_blank"}
                rel="noreferrer"
              >
                РП ВЧ.05. Экономика и управление на транспорте 23.02.01, 2022
                г.п., РУП 3
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>9</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={istor1}
                target={"_blank"}
                rel="noreferrer"
              >
                РП ВЧ.06. Теория и устройство судна 23.02.01, 2022 г.п., РУП 3
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>10</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={istor2}
                target={"_blank"}
                rel="noreferrer"
              >
                РП ЕН.01. Математика 23.02.01, 2022 г.п., РУП 3
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>11</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={literatura}
                target={"_blank"}
                rel="noreferrer"
              >
                РП ЕН.02. Информатика 23.02.01, 2022 г.п., РУП 3
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>12</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={matem1}
                target={"_blank"}
                rel="noreferrer"
              >
                РП Математика 1 курс 23.02.01, 2022 г.п., РУП 3
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>13</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={matem2}
                target={"_blank"}
                rel="noreferrer"
              >
                РП Информатика 1 курс 23.02.01, 2022 г.п., РУП 32
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>14</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={metrol}
                target={"_blank"}
                rel="noreferrer"
              >
                РП ОБЖ 23.02.01, 2022 г.п., РУП 2
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>15</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={obj}
                target={"_blank"}
                rel="noreferrer"
              >
                {" "}
                РП ОГСЭ.04. Физическая культура 23.02.01, 2022 г.п., РУП 3
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>16</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={osnov_filos1}
                target={"_blank"}
                rel="noreferrer"
              >
                РП ОП.01. Инженерная графика 23.02.01, 2022 г.п., РУП 3
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>17</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={osnov_fin}
                target={"_blank"}
                rel="noreferrer"
              >
                РП ОП.02. Электротехника и электроника 23.02.01, 2022 г.п., РУП
                3
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>18</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={ohr_truda}
                target={"_blank"}
                rel="noreferrer"
              >
                РП ОП.03.Метрология, стандартизация и сертификация 23.02.01,
                2021 г.п., РУП 3
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>19</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={pm1}
                target={"_blank"}
                rel="noreferrer"
              >
                РП ОП.04 Транспортная система России 23.02.01, 2022 г.п., РУП 3
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>20</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={pm2}
                target={"_blank"}
                rel="noreferrer"
              >
                РП ОП.05. Технические средства (по видам транспорта) 23.02.01,
                2021 г.п., РУП 3
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>21</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={pm3}
                target={"_blank"}
                rel="noreferrer"
              >
                РП ОП.06. ПОПД 23.02.01, 2022 г.п., РУП 3
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>22</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={pm4}
                target={"_blank"}
                rel="noreferrer"
              >
                РП ОП.07. Охрана труда 23.02.01, 2022 г.п., РУП 3
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>23</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={pm02}
                target={"_blank"}
                rel="noreferrer"
              >
                РП ОП.08. Безопасность жизнедеятельности 23.02.01, 2022 г.п.,
                РУП 3
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>24</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={psiholog}
                target={"_blank"}
                rel="noreferrer"
              >
                РП Охрана труда 23.02.01 2021 г.п.
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>25</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={rodnoy}
                target={"_blank"}
                rel="noreferrer"
              >
                РП Физика 23.02.01, 2022 г.п., РУП 3
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>26</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={russkiy}
                target={"_blank"}
                rel="noreferrer"
              >
                РП Физкультура 1 курс 23.02.01, 2022 г.п., РУП 3
              </a>
            </td>
            <td></td>
          </tr>
          <tr>
            <td>27</td>
            <td>
              {" "}
              <PDF height={"25px"} width={"25px"} />
              <a
                itemProp="educationRpd"
                href={tus}
                target={"_blank"}
                rel="noreferrer"
              >
                РП Химия 23.02.01, 2022 г.п., РУП 3
              </a>
            </td>
            <td></td>
          </tr>
        </tbody>
      </table>
    </section>
  );
};
export default OrganizationOfTransportation2022;
