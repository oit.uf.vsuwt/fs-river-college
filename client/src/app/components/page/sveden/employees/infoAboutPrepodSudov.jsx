import React from "react";
const InfoAboutPrepodSudov = () => {
  return (
    <>
      {" "}
      <tr>
        <td colSpan="12">26.02.03 «Судовождение» (углубленная подготовка)</td>
      </tr>
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Абкадиров Ильдар Ангамович</td>
        <td itemProp="post">
          Преподаватель, первая квалификационная категория
        </td>
        <td itemProp="teachingDiscipline">
          Технические средства судовождения. Судовое радиоборудование.
          Мореходная астрономия. Тренажерная подготовка. Использование ЭКНИС,
          РЛС, САРП. Оператор связи ГМССБ. Квалификационный экзамен
        </td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">Инженер-судоводитель</td>
        <td itemProp="employeeQualification">Судовождение на морских путях</td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          "Особенности деятельности педагогических работников СПО в свете
          требований профессионального стандарта"(2020); "Обучение работников по
          оказанию первой помощи пострадавшим"(2023)
        </td>
        <td itemProp="genExperience">27</td>
        <td itemProp="specExperience">27</td>
        <td itemProp="teachingOp">
          26.02.03 «Судовождение» (углубленная подготовка)
        </td>
      </tr>
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Адиятуллина Диля Миннемуслимовна</td>
        <td itemProp="post">Социальный педагог, преподаватель</td>
        <td itemProp="teachingDiscipline">Психология общения</td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">"Социальный педагог, педагог психолог"</td>
        <td itemProp="employeeQualification">Социальная педагогика</td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          "НЛП практик"(2022); "Профилактическая работа в деятельности
          педагога-психолога образовательной организации" (2021); "Обучение
          работников по оказанию первой помощи пострадавшим"(2023)
        </td>
        <td itemProp="genExperience">12</td>
        <td itemProp="specExperience">11</td>
        <td itemProp="teachingOp">
          26.02.03 «Судовождение» (углубленная подготовка)
        </td>
      </tr>
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Акбарова Залия Шамсуновна</td>
        <td itemProp="post">
          Преподаватель, высшая квалификационная категория
        </td>
        <td itemProp="teachingDiscipline">Математика</td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">Учитель физики и математики</td>
        <td itemProp="employeeQualification">Физика и математика</td>
        <td itemProp="degree">
          Ученая степень Кандидат педагогических наук 13.00.08 "Теория и
          методика профессионального образования"
        </td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          {" "}
          " Методика преподавания математики в СПО в условиях реализации ФГОС
          СПО" (2022); "Обучение работников по оказанию первой помощи
          пострадавшим"(2023)
        </td>
        <td itemProp="genExperience">24</td>
        <td itemProp="specExperience">24</td>
        <td itemProp="teachingOp">
          26.02.03 «Судовождение» (углубленная подготовка)
        </td>
      </tr>
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Акбашева Альбина Галимзяновна</td>
        <td itemProp="post">
          Заместитель директора по воспитательной работе, преподаватель
        </td>
        <td itemProp="teachingDiscipline">ПОПД</td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">Магистр</td>
        <td itemProp="employeeQualification">Экономика</td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          "Методические подходы к формированию образовательной программы в
          рамках перехода на ФГОС-3++"(2021); Современные аспекты молодежной
          политики и организации безопасности в вузах"(2022);. "Обучение
          работников по оказанию первой помощи пострадавшим"(2023)
        </td>
        <td itemProp="genExperience">14</td>
        <td itemProp="specExperience">13</td>
        <td itemProp="teachingOp">
          26.02.03 «Судовождение» (углубленная подготовка)
        </td>
      </tr>
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Акбердин Венер Минисламович</td>
        <td itemProp="post">Воспитатель, преподаватель</td>
        <td itemProp="teachingDiscipline">ОБЖ Основы обороны государства</td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">
          Инженер по эксплуатации радиотехнических средств
        </td>
        <td itemProp="employeeQualification">
          "Командная тактическая специальность войсковая противовоздушня оборона
          сухопутных войск"
        </td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          "Обучение работников по оказанию первой помощи пострадавшим" (2023)
        </td>
        <td itemProp="genExperience">35</td>
        <td itemProp="specExperience">5</td>
        <td itemProp="teachingOp">
          26.02.03 «Судовождение» (углубленная подготовка)
        </td>
      </tr>
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Арсланова Наталья Анатольевна</td>
        <td itemProp="post">Медицинская сестра, преподаватель</td>
        <td itemProp="teachingDiscipline">ОБЖ. Основы мед. знаний</td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">
          "Лингвист, переводчик немецкого и английского языка. Медицинская
          сестра"
        </td>
        <td itemProp="employeeQualification">
          "Перевод и переводоведение. Сестринское дело"
        </td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          {" "}
          "Охрана здоровья работников промышленных и других
          предприятий(медсестра здравпункта промышленных и других
          предприятий"(2022);"Цифровые технологии в преподавании профильных
          дисциплин"(2022);"Подготовка преподавателей, обучающих приемам
          оказания первой помощи" (2023)
        </td>
        <td itemProp="genExperience">14</td>
        <td itemProp="specExperience">3</td>
        <td itemProp="teachingOp">
          26.02.03 «Судовождение» (углубленная подготовка)
        </td>
      </tr>
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Ахмадеева Фарида Шариповна</td>
        <td itemProp="post">
          Директор, преподаватель, высшая квалификационная категория
        </td>
        <td itemProp="teachingDiscipline">
          Технология перевозки грузов. Квалификационный экзамен
        </td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">Инженер водного транспорта</td>
        <td itemProp="employeeQualification">
          Организация перевозок и управление на транспорте (водном)
        </td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          "Особенности деятельности педагогических работников СПО в свете
          требований профессионального стандарта"(2020); "Обучение работников по
          оказанию первой помощи пострадавшим"(2023)"
        </td>
        <td itemProp="genExperience">38</td>
        <td itemProp="specExperience">34</td>
        <td itemProp="teachingOp">
          26.02.03 «Судовождение» (углубленная подготовка)
        </td>
      </tr>
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Ахметшин Максут Раянович</td>
        <td itemProp="post">
          Преподаватель, первая квалификационная категория
        </td>
        <td itemProp="teachingDiscipline">
          Метрология и стандартизация. Механика
        </td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">"Инженер-механик. Экономист"</td>
        <td itemProp="employeeQualification">
          "Механизация сельского хозяйства, Финансы и кредит"
        </td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          "Особенности деятельности педагогических работников СПО в свете
          требований профессионального стандарта"(2020);"Конструирование и
          расчет с применением САПР Компас-3D на занятиях общепрофессиональных
          дисциплин" (2022); Прикладной искусственный интеллект в программах
          дисциплин"(2022); "Обучение работников по оказанию первой помощи
          пострадавшим"(2023)
        </td>
        <td itemProp="genExperience">31</td>
        <td itemProp="specExperience">15</td>
        <td itemProp="teachingOp">
          26.02.03 «Судовождение» (углубленная подготовка)
        </td>
      </tr>
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Баширова Альфия Анваровна</td>
        <td itemProp="post">Педагог-организатор, преподаватель</td>
        <td itemProp="teachingDiscipline">География</td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">Учитель башкирского языка и литературы</td>
        <td itemProp="employeeQualification">Филология</td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          "Методические аспекты при изучении русской литературы последней трети
          XIX века в современной школе"(2020); Переподготовка:"География:теория
          и методика преподавания в профессиональном образовании"(2023);
          "Обучение работников по оказанию первой помощи пострадавшим"(2023)
        </td>
        <td itemProp="genExperience">16</td>
        <td itemProp="specExperience">3</td>
        <td itemProp="teachingOp">
          26.02.03 «Судовождение» (углубленная подготовка)
        </td>
      </tr>
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Байсаков Фатых Тимерказыкович</td>
        <td itemProp="post">Преподаватель, высшая квалификационая категория</td>
        <td itemProp="teachingDiscipline">Электроника и электротехника.</td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">
          Учитель математики и физики средней школы
        </td>
        <td itemProp="employeeQualification">Математика и физика</td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          Переподготовка:"Электроэнергетические системы и сети"(2016); "Обучение
          работников по оказанию первой помощи пострадавшим"(2023)
        </td>
        <td itemProp="genExperience">34</td>
        <td itemProp="specExperience">34</td>
        <td itemProp="teachingOp">
          26.02.03 «Судовождение» (углубленная подготовка)
        </td>
      </tr>
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Белобородова Наталья Павловна</td>
        <td itemProp="post">
          Начальник отдела конвенционной подготовки и дополнительного
          образования. Заслуженный учитель РБ
        </td>
        <td itemProp="teachingDiscipline">Инженерная графика</td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">"Инженер-механик"</td>
        <td itemProp="employeeQualification">Авиационные двигатели</td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          "Обучение работников по оказанию первой помощи пострадавшим" (2023)
        </td>
        <td itemProp="genExperience">42</td>
        <td itemProp="specExperience">33</td>
        <td itemProp="teachingOp">
          26.02.03 «Судовождение» (углубленная подготовка)
        </td>
      </tr>
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Бублис Юрий Федорович</td>
        <td itemProp="post">
          Преподаватель, высшая квалификационная категория
        </td>
        <td itemProp="teachingDiscipline">
          Теория устройства судна. СВМ и системы. Обслуживание и ремонт судовых
          энергетических установок
        </td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">Инженер кораблестроитель</td>
        <td itemProp="employeeQualification">Судостроение и судоремонт</td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          "Педагогика и психология в системе СПО" (2021); Стажировка: изучил
          специфику работ по обработке и размещению груза,управлению
          барже-буксирным составом,использованию технических средств
          судовождения,конструкцию корпуса, судовые вспомогательные механизмы и
          устройства, общую и специальную лоцию р. Белой.( 2021); "Обучение
          работников по оказанию первой помощи пострадавшим"(2023)
        </td>
        <td itemProp="genExperience">50</td>
        <td itemProp="specExperience">28</td>
        <td itemProp="teachingOp">
          26.02.03 «Судовождение» (углубленная подготовка)
        </td>
      </tr>
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Гайнетдинова Эльвера Галихановна</td>
        <td itemProp="post">
          Преподаватель, высшая квалификационная категория
        </td>
        <td itemProp="teachingDiscipline">
          Материаловедение. Судовые энргетические установки /включая тренажер
          вахтенного механика. Квалификационный экзамен.
        </td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">Инженер-механик</td>
        <td itemProp="employeeQualification">«Судовые машины и механизмы»</td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          "Педагогика и психология в системе СПО" (2021); Стажировка: изучил
          специфику работ по обработке и размещению груза,управлению
          барже-буксирным составом,использованию технических средств
          судовождения,конструкцию корпуса, судовые вспомогательные механизмы и
          устройства, общую и специальную лоцию р. Белой.( 2021); "Обучение
          работников по оказанию первой помощи пострадавшим"(2023)
        </td>
        <td itemProp="genExperience">45</td>
        <td itemProp="specExperience">43</td>
        <td itemProp="teachingOp">
          26.02.03 «Судовождение» (углубленная подготовка)
        </td>
      </tr>
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Галеева Алина Вадимовна</td>
        <td itemProp="post">Педагог-организатор, прподаватель</td>
        <td itemProp="teachingDiscipline">
          Основы финансовой грамотности и препринимательской деятельности
        </td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">Экономист</td>
        <td itemProp="employeeQualification">Финансы и кредит</td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          "Обучение работников по оказанию первой помощи пострадавшим"(2023)
        </td>
        <td itemProp="genExperience">8</td>
        <td itemProp="specExperience">2</td>
        <td itemProp="teachingOp">
          26.02.03 «Судовождение» (углубленная подготовка)
        </td>
      </tr>
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Галимова Альмира Фагимовна</td>
        <td itemProp="post">
          Преподаватель, высшая квалификационная категория
        </td>
        <td itemProp="teachingDiscipline">Математика</td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">Учитель математики</td>
        <td itemProp="employeeQualification">Математика</td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          "Преподавание математики в условиях реализации ФГОС" (дистанционно)
          2022; " Разработка, внедерние и сертификация системы менеджмента
          качества организации"(2022);"Обучение работников по оказанию первой
          помощи пострадавшим"(2023)
        </td>
        <td itemProp="genExperience">19</td>
        <td itemProp="specExperience">19</td>
        <td itemProp="teachingOp">
          26.02.03 «Судовождение» (углубленная подготовка)
        </td>
      </tr>
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Галлямова Раушания Харисовна</td>
        <td itemProp="post">
          Преподаватель, высшая квалификационная категория
        </td>
        <td itemProp="teachingDiscipline">Физика</td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">Физик</td>
        <td itemProp="employeeQualification">Физика</td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          "Обучение работников по оказанию первой помощи пострадавшим" (2023)
        </td>
        <td itemProp="genExperience">29</td>
        <td itemProp="specExperience">23</td>
        <td itemProp="teachingOp">
          26.02.03 «Судовождение» (углубленная подготовка)
        </td>
      </tr>
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Гарифуллина Зульфия Муратовна</td>
        <td itemProp="post">
          Преподаватель, первая квалификационная категория
        </td>
        <td itemProp="teachingDiscipline">
          Иностранный язык в профессиональной деятельности
        </td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">
          Учитель башкирского языка, литературы и английского языка.
        </td>
        <td itemProp="employeeQualification">
          Родной язык, литература и иностранный язык
        </td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          "Обучение работников по оказанию первой помощи пострадавшим"(2023)
        </td>
        <td itemProp="genExperience">12</td>
        <td itemProp="specExperience">8</td>
        <td itemProp="teachingOp">
          26.02.03 «Судовождение» (углубленная подготовка)
        </td>
      </tr>
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Еникеев Алик Гайсинович</td>
        <td itemProp="post">
          Начальник отдела практической подготовки и трудоустройства
          выпускников, преподаватель, высшая квалификационная категория
        </td>
        <td itemProp="teachingDiscipline">
          Мореходная астрономия. Теория устройства судна. Навигационная
          гидрометеорология. Тренажерная подгготовка. Исспользование РЛС, САРП.
          Управление судном. Правила плавания и управление судами на ВВП.
          Технические средства судовождения. Оператор связи ГМССБ
        </td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">Инженер</td>
        <td itemProp="employeeQualification">Судовождение</td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          "Оказание первой помощи "(2020); " Особенности деятельности
          педагогических работников СПО в свете требований профессионального
          стандарта"(2020) "Организация и и проведение демонстрационного
          экзамена по стандартам WorIdSkills Russia в 2021 году"; Работа на
          судне в качестве руководителя практики ( 2021); Стажировка : изучил
          специфику работ по перевозке и управлению неорганизованной массой
          людей, подготовки по безопасности персонала, обеспечивающего
          непосредственное обслуживание пассажиров в пассажирскимх помещениях;
          использованию технических средств судовождения; конструкции
          корпуса,судовых вспомогательнеых механизмов и оборудованию МКО; общую
          и специальную линию р. Белой. (2022) ; Цифровые технологии в
          преподавании профильных дисциплин (2022)
        </td>
        <td itemProp="genExperience">21</td>
        <td itemProp="specExperience">8</td>
        <td itemProp="teachingOp">
          26.02.03 «Судовождение» (углубленная подготовка)
        </td>
      </tr>
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Жукова Жанар Сергеевна</td>
        <td itemProp="post">Преподаватель</td>
        <td itemProp="teachingDiscipline">
          Иностранный язык в профессиональной деятельности.
        </td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">
          Учитель английского и французского языка
        </td>
        <td itemProp="employeeQualification">
          Иностранный язык /английский/ с дополнительной специальностью
          /французский/
        </td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          "Методика преподавания русского языка как иностранного"(2022);
          "Обучение работников по оказанию первой помощи пострадавшим"(2023)
        </td>
        <td itemProp="genExperience">7</td>
        <td itemProp="specExperience">7</td>
        <td itemProp="teachingOp">
          26.02.03 «Судовождение» (углубленная подготовка)
        </td>
      </tr>
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Зараев Ильшат Фаритович</td>
        <td itemProp="post">
          Преподаватель, первая квалификационная категория
        </td>
        <td itemProp="teachingDiscipline">Химия. Биология. География</td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">Учитель биологии и химии</td>
        <td itemProp="employeeQualification">Биология и химия</td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          "Современные образовательные технологии в преподавании химии с учетом
          ФГОС" (2020); "Основы предмета "Экология в соответствии с требованиями
          ФГОС" (2020); "Методика преподавания астрономии при подготовке
          специалистов среднего звена" (2020); "Обучение работников по оказанию
          первой помощи пострадавшим"(2023)
        </td>
        <td itemProp="genExperience">16</td>
        <td itemProp="specExperience">16</td>
        <td itemProp="teachingOp">
          26.02.03 «Судовождение» (углубленная подготовка)
        </td>
      </tr>
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Зкриева Гульнара Робертовна</td>
        <td itemProp="post">Преподаватель высшая квалификационная категория</td>
        <td itemProp="teachingDiscipline">
          Электрооборудование судов. Квалификационный экзамен. Электротехника и
          электроника. Судовое радиооборудование. Устройство и эксплуатация
          судовых энергетических установок. Обслуживание и ремонт судовых
          энергетических установок
        </td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">Инженер-электрик</td>
        <td itemProp="employeeQualification">
          Электрофикация и автоматизация сельского хозяйства
        </td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          "Педагогика и психология в системе СПО" (2021); Стажировка :
          "Электромонтажные работы" (2021); Стажировка: изучила специфику работ
          по электрооборудованию судов: электрических машин, аппаратуры
          управления и защиты, судовых электроприводов и электрических систем
          автоматики и контроля (2021); "Методические подходы к формированию
          образователной программы в рамках переходы на ФГОС-3++" (2021);
          "Обучение работников по оказанию первой помощи пострадавшим"(2023)
        </td>
        <td itemProp="genExperience">29</td>
        <td itemProp="specExperience">28</td>
        <td itemProp="teachingOp">
          26.02.03 «Судовождение» (углубленная подготовка)
        </td>
      </tr>
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Исхакова Лилия Лутфулловна</td>
        <td itemProp="post">
          Преподаватель, первая квалификационная категория
        </td>
        <td itemProp="teachingDiscipline">
          Технология перевозок. Коммерческая эксплуатация. Квалификационный
          экзамен
        </td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">Бакалавр</td>
        <td itemProp="employeeQualification">
          Технология транспортных процессов
        </td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          "Педагогика и психология в системе СПО" (2021); Цифровые технологии в
          преподавании профильных дисциплин (2022);"Обучение работников по
          оказанию первой помощи пострадавшим"(2023)
        </td>
        <td itemProp="genExperience">22</td>
        <td itemProp="specExperience">5</td>
        <td itemProp="teachingOp">
          26.02.03 «Судовождение» (углубленная подготовка)
        </td>
      </tr>
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Козырь Снежана Андреевна</td>
        <td itemProp="post">
          Преподаватель, высшая квалификационная категория
        </td>
        <td itemProp="teachingDiscipline">Русский язык</td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">Бакалавр</td>
        <td itemProp="employeeQualification">Филология</td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          "Особенности деятельности педагогических работников СПО в свете
          требований профессионального стандарта"(2020); "Исследовательская
          технология на уроках русского языка и литературы по ФГОС"(2020);
          "Скрайбинг и веб-инвест как инновационные образовательные технологии в
          условиях реализации ФГОС СПО"(2022);"Обучение работников по оказанию
          первой помощи пострадавшим"(2023)
        </td>
        <td itemProp="genExperience">6</td>
        <td itemProp="specExperience">6</td>
        <td itemProp="teachingOp">
          26.02.03 «Судовождение» (углубленная подготовка)
        </td>
      </tr>{" "}
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Кульмухаметова Гульнара Айдаровна</td>
        <td itemProp="post">Преподаватель</td>
        <td itemProp="teachingDiscipline">
          Техническая термодинамика и теплопередача. Инженерная графика
        </td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">Магистр</td>
        <td itemProp="employeeQualification">
          Технологические машины и обрудования
        </td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          "Методические подходы к формированию образовательной программы в
          рамках перехода на ФГОС-3++"(2021); Цифровые технологии в преподавании
          профильных дисциплин" (2022);"Обучение работников по оказанию первой
          помощи пострадавшим"(2023).
        </td>
        <td itemProp="genExperience">10</td>
        <td itemProp="specExperience">9</td>
        <td itemProp="teachingOp">
          26.02.03 «Судовождение» (углубленная подготовка)
        </td>
      </tr>{" "}
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Котухов Сергей Владимирович</td>
        <td itemProp="post">Преподаватель</td>
        <td itemProp="teachingDiscipline">Новигация и лоция</td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">Инженер-судоводитель</td>
        <td itemProp="employeeQualification">
          Судовождение на внутренних водных путях
        </td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          Повышение квалификации 2017 г. 3 группа судов 2 помошник механика.
          Повышение квалификации 3 группа водоизмещения капитан 3 группы 2017 г.
          Свидетельство тренажерной подготовки ППБ, борьба с огнем 2022 г.
        </td>
        <td itemProp="genExperience">30</td>
        <td itemProp="specExperience">1</td>
        <td itemProp="teachingOp">
          26.02.03 «Судовождение» (углубленная подготовка)
        </td>
      </tr>{" "}
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Крикунов Сергей Петрович</td>
        <td itemProp="post">
          Преподаватель, высшая квалификационная категория
        </td>
        <td itemProp="teachingDiscipline">
          Навигация и лоция. Навигационная гидрометеорология
        </td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">Инженер-судоводитель</td>
        <td itemProp="employeeQualification">
          Судовождение на внутренних водных путях
        </td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          "Педагогика и психология в системе СПО" (2021); "Методические подходы
          к формированию образовательной программы в рамках перехода на
          ФГОС-3++"(2021); "Обучение работников по оказанию первой помощи
          пострадавшим"(2023)
        </td>
        <td itemProp="genExperience">49</td>
        <td itemProp="specExperience">48</td>
        <td itemProp="teachingOp">
          26.02.03 «Судовождение» (углубленная подготовка)
        </td>
      </tr>{" "}
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Муратов Раис Фаритович</td>
        <td itemProp="post">Преподаватель. Отличник речного флота РФ.</td>
        <td itemProp="teachingDiscipline">
          Основы анализа эффективности работы судна с применением информационных
          технологий. Квалификационный экзамен. Оценка эффективности и качества
          работы. Квалификационный экзамен
        </td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">Инженер водного транспорта</td>
        <td itemProp="employeeQualification">
          Эксплуатация водного транспорта
        </td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          "Методические подходы к формированию образовательной программы в
          рамках перехода на ФГОС-3++"(2021); "Обучение работников по оказанию
          первой помощи пострадавшим"(2023)
        </td>
        <td itemProp="genExperience">43</td>
        <td itemProp="specExperience">12</td>
        <td itemProp="teachingOp">
          26.02.03 «Судовождение» (углубленная подготовка)
        </td>
      </tr>{" "}
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Мусина Гульфия Ириковна</td>
        <td itemProp="post">
          Заместитель директора по учебной работе. Преподаватель, высшая
          квалификационная категория
        </td>
        <td itemProp="teachingDiscipline">Литература</td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">Учитель русского языка и литературы</td>
        <td itemProp="employeeQualification">Русский язык и литература</td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          "Профессиональная компетентность педагогов в условиях внедрения ФГОС"
          (2020); "Реализация образовательных программ среднего
          профессионального образования с учетом специфики WordSkills"
          (2021);"Методические подходы к формированию образовательной программы
          в рамках переходы на ФГОС-3++" (2021); "Особенности организации
          изучения учебного предмета"Родная русская литература" с учетом
          обновленного ФГОС СПО"(2023); "Обучение работников по оказанию первой
          помощи пострадавшим"(2023)
        </td>
        <td itemProp="genExperience">28</td>
        <td itemProp="specExperience">28</td>
        <td itemProp="teachingOp">
          26.02.03 «Судовождение» (углубленная подготовка)
        </td>
      </tr>{" "}
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Мухутдинова Лилия Ирековна</td>
        <td itemProp="post">
          Преподаватель, высшая квалификационная категория
        </td>
        <td itemProp="teachingDiscipline">
          Иностранный язык в профессиональной деятельности
        </td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">Филолог. Преподаватель немецкого языка</td>
        <td itemProp="employeeQualification">Немецкий язык. Филология</td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          {" "}
          "Особенности деятельности педагогических работников СПО в свете
          требований профессионального стандарта"(2020); "Специфика преподавания
          английского языка с учетом требований ФГОС"(2018); "Дизайн
          претентаций"(2022);"Обучение работников по оказанию первой помощи
          пострадавшим"(2023)
        </td>
        <td itemProp="genExperience">29</td>
        <td itemProp="specExperience">29</td>
        <td itemProp="teachingOp">
          26.02.03 «Судовождение» (углубленная подготовка)
        </td>
      </tr>{" "}
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Павлова Алина Тахировна</td>
        <td itemProp="post">
          Ведущий специалист отдела практической подготовки и трудоустройства
          выпускников, преподаватель
        </td>
        <td itemProp="teachingDiscipline">Обществознание</td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">Бакалавр</td>
        <td itemProp="employeeQualification">Юриспруденция</td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          "Обучение работников по оказанию первой помощи пострадавшим" (2023)
        </td>
        <td itemProp="genExperience">20</td>
        <td itemProp="specExperience">-</td>
        <td itemProp="teachingOp">
          26.02.03 «Судовождение» (углубленная подготовка)
        </td>
      </tr>{" "}
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Портнягина Ирина Валерьевна</td>
        <td itemProp="post">Преподаватель</td>
        <td itemProp="teachingDiscipline">
          Управление судом. ПП и управление судами на ВВП. Общая и социальная
          лоции ВВП РФ
        </td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">Инженер-судоводитель</td>
        <td itemProp="employeeQualification">
          Судовождение на внутренних водных путях
        </td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          "Цифровые технологии в преподавании профильных дисциплин"
          (2022);"Обучение работников по оказанию первой помощи
          пострадавшим"(2023)
        </td>
        <td itemProp="genExperience">34</td>
        <td itemProp="specExperience">10</td>
        <td itemProp="teachingOp">
          26.02.03 «Судовождение» (углубленная подготовка)
        </td>
      </tr>{" "}
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Рябова Зинаида Николаевна</td>
        <td itemProp="post">Преподаватель</td>
        <td itemProp="teachingDiscipline">Физическая культура</td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">
          Специалист по физической культуре и спорту
        </td>
        <td itemProp="employeeQualification">Физическая культура и спорт</td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          "Обучение работников по оказанию первой помощи пострадавшим"(2023)
        </td>
        <td itemProp="genExperience">18</td>
        <td itemProp="specExperience">18</td>
        <td itemProp="teachingOp">
          26.02.03 «Судовождение» (углубленная подготовка)
        </td>
      </tr>{" "}
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Решетников Виталий Евгеньевич</td>
        <td itemProp="post">Преподаватель</td>
        <td itemProp="teachingDiscipline">История</td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">Историк. Преподаватель истории</td>
        <td itemProp="employeeQualification">История</td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          "Философия: Теория и методика преподавания в условиях реализации ФГОС"
          (2019); "Обучение работников по оказанию первой помощи
          пострадавшим"(2023).
        </td>
        <td itemProp="genExperience">21</td>
        <td itemProp="specExperience">13</td>
        <td itemProp="teachingOp">
          26.02.03 «Судовождение» (углубленная подготовка)
        </td>
      </tr>{" "}
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Романов Зульфир Вахитович</td>
        <td itemProp="post">
          Преподаватель, высшая квалификационная категория
        </td>
        <td itemProp="teachingDiscipline">
          ОБЖ Безопасность жизнедеятельности
        </td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">Историк. Преподаватель истории</td>
        <td itemProp="employeeQualification">История</td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          {" "}
          Профессиональная переподготовка : Гражданская оборона и защита
          населения от чрезвычайных ситуаций для должностных лиц"(2021)
        </td>
        <td itemProp="genExperience">13</td>
        <td itemProp="specExperience">8</td>
        <td itemProp="teachingOp">
          26.02.03 «Судовождение» (углубленная подготовка)
        </td>
      </tr>{" "}
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Сиразетдинова Эльвира Раисовна</td>
        <td itemProp="post">
          Старший мтодист, преподаватель, первая квалификационная категория
        </td>
        <td itemProp="teachingDiscipline">
          Предупреждение и предотвращение загрязнения окружающей среды.
          Квалификационный экзамен.
        </td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">Историк. Преподаватель истории</td>
        <td itemProp="employeeQualification">История</td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          "Особенности деятельности педагогических работников СПО в сфете
          требований профессионального стандарта"(2019); Обеспечение
          экологической безопасности при работах в облас ти обращения с отходами
          1-1У классов опасности"(2020); Переподготовка : Обеспечение
          экологической безопасности руководителями и специалистами
          общехозяйственных систем управления. (2021);Методические подходы к
          формированию образовательной программы в рамках перехода на
          ФГОС-3++"(2021); "Обучение работников по оказанию первой помощи
          пострадавшим"(2023)
        </td>
        <td itemProp="genExperience">24</td>
        <td itemProp="specExperience">18</td>
        <td itemProp="teachingOp">
          26.02.03 «Судовождение» (углубленная подготовка)
        </td>
      </tr>{" "}
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Уткина Елена Михайловна</td>
        <td itemProp="post">Преподаватель</td>
        <td itemProp="teachingDiscipline">
          Обеспечение безопасности плавания и живучести судна. БЖ на судне.
          Транспортная безопасность на судне. Квалификационный экзамен.
        </td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">Инженер</td>
        <td itemProp="employeeQualification">Судовождение на ВВП</td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          "Обучение работников по оказанию первой помощи пострадавшим"(2023)
        </td>
        <td itemProp="genExperience">36</td>
        <td itemProp="specExperience">27</td>
        <td itemProp="teachingOp">
          26.02.03 «Судовождение» (углубленная подготовка)
        </td>
      </tr>
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Тарба Растан Александрович</td>
        <td itemProp="post">
          Преподаватель, первая квалификационная категория
        </td>
        <td itemProp="teachingDiscipline">Устройство и эксплуатации судов</td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">"Техник-судомеханик. Бакалавр"</td>
        <td itemProp="employeeQualification">
          "Технологические машины и оборудование. Эксплуатация транспортных
          энергетических установок"
        </td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          {" "}
          "Педагогика и психология в системе СПО" (2021); Стажировка: изучил
          специфику работы и несения вахты мотористов, судовые энергетические
          установки ти вспомогательные механизмы машинного отделения, палубные
          вспомогательные механизмы (2021);Методические подходы к формированию
          образовательной программы в рамках перехода на ФГОС-3++"(2021);
          "Обучение работников по оказанию первой помощи пострадавшим"(2023)
        </td>
        <td itemProp="genExperience">10</td>
        <td itemProp="specExperience">7</td>
        <td itemProp="teachingOp">
          26.02.03 «Судовождение» (углубленная подготовка)
        </td>
      </tr>
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Филлипова Снежана Васильевна</td>
        <td itemProp="post">Преподаватель</td>
        <td itemProp="teachingDiscipline">
          Технология перевозок. Оптимальные варианты планирования рейса судна.
          Основы анализа эффективности работы судна с применением информационных
          систем. Физическая культура
        </td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">
          Инженер транспортных и технологических машин и оборудования
          /нефтегазодобыча/
        </td>
        <td itemProp="employeeQualification">
          Сервис транспортных и технологических машин и оборудования
        </td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          Переподготовка:"Физкультурно-оздоровительная и спортивно-массовая
          работа с населением"(2020);"Обучение работников по оказанию первой
          помощи" пострадавшим"(2023)
        </td>
        <td itemProp="genExperience">11</td>
        <td itemProp="specExperience">6</td>
        <td itemProp="teachingOp">
          26.02.03 «Судовождение» (углубленная подготовка)
        </td>
      </tr>
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Филлипов Эдуард Валерьевич</td>
        <td itemProp="post">Преподаватель</td>
        <td itemProp="teachingDiscipline">Физическая культура</td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">Инженер-экономист</td>
        <td itemProp="employeeQualification">
          Экономика и управление на предприятиях
        </td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          Переподготовка: "Педагог по физической культуре и спорту" (2016);
        </td>
        <td itemProp="genExperience">20</td>
        <td itemProp="specExperience">-</td>
        <td itemProp="teachingOp">
          26.02.03 «Судовождение» (углубленная подготовка)
        </td>
      </tr>
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Фомичев Сергей Юрьевич</td>
        <td itemProp="post">Преподаватель</td>
        <td itemProp="teachingDiscipline">
          Судовая автоматика и КИП. Обслуживание и ремонт судового
          электрического и электронного оборудования. Квалифицированный экзамен
        </td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">Инженер-электрик</td>
        <td itemProp="employeeQualification">
          Электропривод и автоматизация промышленных установок
        </td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          Методические подходы к формированию образовательной программы в рамках
          перехода на ФГОС-3++"(2021); "Обучение работников по оказанию первой
          помощи пострадавшим"(2023)
        </td>
        <td itemProp="genExperience">39</td>
        <td itemProp="specExperience">3</td>
        <td itemProp="teachingOp">
          26.02.03 «Судовождение» (углубленная подготовка)
        </td>
      </tr>
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Щербакова Амина Байраковна</td>
        <td itemProp="post">
          Заведующий учебным отделом, преподаватель высшей квалификационной
          категории
        </td>
        <td itemProp="teachingDiscipline">Обществознание</td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">
          Историк, преподаватель истории и обществоведения
        </td>
        <td itemProp="employeeQualification">История</td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          {" "}
          Методические подходы к формированию образовательной программы в рамках
          перехода на ФГОС-3++"(2021); "Совершенствование процесса преподавания
          истории и обществознания в основной школе в условиях реализации
          обновленного ФГОС"(2023); "Обучение работников по оказанию первой
          помощи" пострадавшим"(2023)
        </td>
        <td itemProp="genExperience">30</td>
        <td itemProp="specExperience">30</td>
        <td itemProp="teachingOp">
          26.02.03 «Судовождение» (углубленная подготовка)
        </td>
      </tr>
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Бакиров Урал Юлаевич</td>
        <td itemProp="post"> Преподаватель, внешний совместитель (БРВПС)</td>
        <td itemProp="teachingDiscipline">
          Подготовка по бортьбе с пожаром. Навигационная гидрометеорология.
          Технические средства судовождения
        </td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">Техник-судомеханик</td>
        <td itemProp="employeeQualification">
          Судовождение на внутренних водных путях
        </td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">-</td>
        <td itemProp="genExperience">7</td>
        <td itemProp="specExperience">2</td>
        <td itemProp="teachingOp">
          26.02.03 «Судовождение» (углубленная подготовка)
        </td>
      </tr>
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Строев Николай Александрович</td>
        <td itemProp="post"> Преподаватель, внешний совместитель (БРВПС)</td>
        <td itemProp="teachingDiscipline">
          Судовые работы и основы судовождения. Квалификационный экзамен
        </td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">Техник-судоводитель</td>
        <td itemProp="employeeQualification">
          Судовождение на внутренних водных путях
        </td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">-</td>
        <td itemProp="genExperience">15</td>
        <td itemProp="specExperience">2</td>
        <td itemProp="teachingOp">
          26.02.03 «Судовождение» (углубленная подготовка)
        </td>
      </tr>
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Малофеев Алексей Сергеевич</td>
        <td itemProp="post">"Преподаватель. внешний совместитель (БРВПС)"</td>
        <td itemProp="teachingDiscipline">
          Судовые энергетические установки и их эксплуатация /включая тренажер
          вахтенного механизма/
        </td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">Техник-судоводитель</td>
        <td itemProp="employeeQualification">
          Судовождение на внутренних водных путях
        </td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">-</td>
        <td itemProp="genExperience">10</td>
        <td itemProp="specExperience">2</td>
        <td itemProp="teachingOp">
          26.02.03 «Судовождение» (углубленная подготовка)
        </td>
      </tr>{" "}
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Иванов Генрих Николаевич</td>
        <td itemProp="post"> Преподаватель, внешний совместитель (БРВПС)</td>
        <td itemProp="teachingDiscipline">
          Судовые энергетические установки /включая тренажер вахтенного
          механизма/
        </td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">Техник-судоводитель</td>
        <td itemProp="employeeQualification">
          Судовождение на внутренних водных путях
        </td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">-</td>
        <td itemProp="genExperience">26</td>
        <td itemProp="specExperience">2</td>
        <td itemProp="teachingOp">
          26.02.03 «Судовождение» (углубленная подготовка)
        </td>
      </tr>{" "}
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Токмаков Александр Александрович</td>
        <td itemProp="post">
          Договор гражданско-правового характера (Бельская инспекция
          Государственного портового контроля ФБУ Администрации "Камводпуть")
        </td>
        <td itemProp="teachingDiscipline">Председатель ГЭК</td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">"Менеджер Техник судоводитель"</td>
        <td itemProp="employeeQualification">
          "Экономика и менеджмент, управление муниципальными организациями
          Судовождение на ВВП"
        </td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">-</td>
        <td itemProp="genExperience">37</td>
        <td itemProp="specExperience">4</td>
        <td itemProp="teachingOp">
          26.02.03 «Судовождение» (углубленная подготовка)
        </td>
      </tr>{" "}
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Решетников Станислав Евгеньевич</td>
        <td itemProp="post">"Преподавтель, ГПХ (Газпромтрансгаз Уфа)"</td>
        <td itemProp="teachingDiscipline">
          Председатель квалификационной комиссии
        </td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">Инженер-судоводитель</td>
        <td itemProp="employeeQualification">Судовождение на ВВП</td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">-</td>
        <td itemProp="genExperience">40</td>
        <td itemProp="specExperience">5</td>
        <td itemProp="teachingOp">
          26.02.03 «Судовождение» (углубленная подготовка)
        </td>
      </tr>{" "}
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Федорова Нина Вячеславовна</td>
        <td itemProp="post">
          Договор гражданско-правового характера (Судоремонтый-судостроительный
          завод)
        </td>
        <td itemProp="teachingDiscipline">
          Председатель квалификационной комиссии
        </td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">Бакалавр</td>
        <td itemProp="employeeQualification">
          Технология транспортных процессов
        </td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">-</td>
        <td itemProp="genExperience">12</td>
        <td itemProp="specExperience">3</td>
        <td itemProp="teachingOp">
          26.02.03 «Судовождение» (углубленная подготовка)
        </td>
      </tr>{" "}
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Аминов Завиль Гильмиярович</td>
        <td itemProp="post">
          {" "}
          Договор гражданско-правового характера ("Волготанкер")
        </td>
        <td itemProp="teachingDiscipline">
          Председатель квалификационной комиссии
        </td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">Инженер-судоводитель</td>
        <td itemProp="employeeQualification">Судовождение на ВВП</td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">-</td>
        <td itemProp="genExperience">38</td>
        <td itemProp="specExperience">2</td>
        <td itemProp="teachingOp">
          26.02.03 «Судовождение» (углубленная подготовка)
        </td>
      </tr>{" "}
    </>
  );
};
export default InfoAboutPrepodSudov;
