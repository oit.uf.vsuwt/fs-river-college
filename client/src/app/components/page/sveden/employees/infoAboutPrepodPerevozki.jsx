import React from "react";
const InfoAboutPrepodPerevozki = () => {
  return (
    <>
      <tr>
        <td colSpan="12">
          23.02.01 "Организация перевозок и управление на транспорте (по видам)"
        </td>
      </tr>
      <tr itemProp="teachingStaff">
        ы<td itemProp="fio">Акбарова Залия Шамсуновна</td>
        <td itemProp="post">
          Преподаватель, высшая квалификационная категория
        </td>
        <td itemProp="teachingDiscipline">Математика</td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">Учитель физики и математики</td>
        <td itemProp="employeeQualification">Физика и математика</td>
        <td itemProp="degree">
          Ученая степень Кандидат педагогических наук 13.00.08 "Теория и
          методика профессионального образования"
        </td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          "Методика преподавания математики в СПО в условиях реализации ФГОС
          СПО" (2022); "Обучение работников по оказанию первой помощи
          пострадавшим"(2023)
        </td>
        <td itemProp="genExperience">24</td>
        <td itemProp="specExperience">24</td>
        <td itemProp="teachingOp">
          23.02.01 "Организация перевозок и управление на транспорте (по видам)"
        </td>
      </tr>
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Акбердин Венер Минисламович</td>
        <td itemProp="post">Воспитатель, преподаватель</td>
        <td itemProp="teachingDiscipline">ОБЖ Основы обороны государства</td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">
          Инженер по эксплуатации радиотехнических средств
        </td>
        <td itemProp="employeeQualification">
          "Командная тактическая специальность войсковая противовоздушня оборона
          сухопутных войск"
        </td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          "Обучение работников по оказанию первой помощи пострадавшим" (2023)
        </td>
        <td itemProp="genExperience">35</td>
        <td itemProp="specExperience">5</td>
        <td itemProp="teachingOp">
          23.02.01 "Организация перевозок и управление на транспорте (по видам)"
        </td>
      </tr>
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Арсланова Наталья Анатольевна</td>
        <td itemProp="post">Медицинская сестра, преподаватель</td>
        <td itemProp="teachingDiscipline">ОБЖ. Основы медицинских знаний</td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">
          "Лингвист, переводчик немецкого и английского языка. Медицинская
          сестра"
        </td>
        <td itemProp="employeeQualification">
          "Перевод и переводоведение. Сестринское дело"
        </td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          "Охрана здоровья работников промышленных и других
          предприятий(медсестра здравпункта промышленных и других
          предприятий"(2022);"Цифровые технологии в преподавании профильных
          дисциплин"(2022);"Подготовка преподавателей, обучающих приемам
          оказания первой помощи" (2023)
        </td>
        <td itemProp="genExperience">14</td>
        <td itemProp="specExperience">3</td>
        <td itemProp="teachingOp">
          23.02.01 "Организация перевозок и управление на транспорте (по видам)"
        </td>
      </tr>
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Ахметшин Максут Раянович</td>
        <td itemProp="post">
          Преподаватель, первая квалификационная категория
        </td>
        <td itemProp="teachingDiscipline">
          Инженерная графика. Механика. Метрология, стандартизация и
          сертификация
        </td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">"Инженер-механик. Экономист"</td>
        <td itemProp="employeeQualification">
          "Механизация сельского хозяйства, Финансы и кредит"
        </td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          "Особенности деятельности педагогических работников СПО в свете
          требований профессионального стандарта"(2020);"Конструирование и
          расчет с применением САПР Компас-3D на занятиях общепрофессиональных
          дисциплин" (2022); Прикладной искусственный интеллект в программах
          дисциплин"(2022); "Обучение работников по оказанию первой помощи
          пострадавшим"(2023)
        </td>
        <td itemProp="genExperience">31</td>
        <td itemProp="specExperience">15</td>
        <td itemProp="teachingOp">
          23.02.01 "Организация перевозок и управление на транспорте (по видам)"
        </td>
      </tr>
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Бакиров Урал Юлаевич</td>
        <td itemProp="post">Преподаватель, внешний совместитель (БРВПС)</td>
        <td itemProp="teachingDiscipline">
          Подготовка по бортьбе с пожаром. Навигационная гидрометеорология.
          Технические средства судовождения
        </td>
        <td itemProp="teachingLevel">
          Среднее профессиональное образование, специалист
        </td>
        <td itemProp="teachingQual">Техник-судомеханик</td>
        <td itemProp="employeeQualification">
          Судовождение на внутренних водных путях
        </td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">-</td>
        <td itemProp="genExperience">7</td>
        <td itemProp="specExperience">2</td>
        <td itemProp="teachingOp">
          23.02.01 "Организация перевозок и управление на транспорте (по видам)"
        </td>
      </tr>{" "}
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Байсаков Фатых Тимерказыкович</td>
        <td itemProp="post">Преподаватель, высшая квалификационая категория</td>
        <td itemProp="teachingDiscipline">Электроника и электротехника</td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">
          Учитель математики и физики средней школы
        </td>
        <td itemProp="employeeQualification">Математика и физика</td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          Переподготовка:""Электроэнергетические системы и сети"(2016);
          "Обучение работников по оказанию первой помощи пострадавшим"(2023)
        </td>
        <td itemProp="genExperience">34</td>
        <td itemProp="specExperience">34</td>
        <td itemProp="teachingOp">
          23.02.01 "Организация перевозок и управление на транспорте (по видам)"
        </td>
      </tr>{" "}
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Бровко Вальдемар Дмитриевич</td>
        <td itemProp="post">Преподаватель</td>
        <td itemProp="teachingDiscipline">Охрана труда</td>
        <td itemProp="teachingLevel">Н/Высшее образование</td>
        <td itemProp="teachingQual">-</td>
        <td itemProp="employeeQualification">Электротехнические устройства</td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          Переподготовка:"Маркетинг и менеджмент в сфере малого и среднего
          бизнеса. Транспортная логистика и складское хозяйство"(2013);
          Стажировка: освоение профессиональных компетенций по темам"
          Эксплуатация судовых технических средств в соответствии с
          установленными правилами,предотвращающими загрезнение окружающей
          среды" и "Организация работы структурного подразделения (2023);
          "Основы безопасности жизнедеятельности" (2023);"Обучение работников по
          оказанию первой помощи пострадавшим"(2023)
        </td>
        <td itemProp="genExperience">28</td>
        <td itemProp="specExperience">1</td>
        <td itemProp="teachingOp">
          23.02.01 "Организация перевозок и управление на транспорте (по видам)"
        </td>
      </tr>{" "}
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Еникеев Алик Гайсинович</td>
        <td itemProp="post">
          Начальник отдела практической подготовки и трудоустройства
          выпускников, преподаватель, высшая квалификационная категория
        </td>
        <td itemProp="teachingDiscipline">Теория устройства судна</td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">Инженер</td>
        <td itemProp="employeeQualification">Судовождение</td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          "Оказание первой помощи "(2020); " Особенности деятельности
          педагогических работников СПО в свете требований профессионального
          стандарта"(2020) "Организация и и проведение демонстрационного
          экзамена по стандартам WorIdSkills Russia в 2021 году"; Работа на
          судне в качестве руководителя практики (2021);"Методические подходы к
          формированию образовательной программы в рамках перехода на ФГОС
          -3++"(2021); Стажировка : изучил специфику работ по перевозке и
          управлению неорганизованной массой людей, подготовки по безопасности
          персонала, обеспечивающего непосредственное обслуживание пассажиров в
          пассажирскимх помещениях; использованию технических средств
          судовождения; конструкции корпуса,судовых вспомогательнеых механизмов
          и оборудованию МКО; общую и специальную линию р. Белой. (2022) ;
          Цифровые технологии в преподавании профильных дисциплин (2022)
        </td>
        <td itemProp="genExperience">21</td>
        <td itemProp="specExperience">8</td>
        <td itemProp="teachingOp">
          23.02.01 "Организация перевозок и управление на транспорте (по видам)"
        </td>
      </tr>{" "}
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Бублис Юрий Федорович</td>
        <td itemProp="post">
          Преподаватель, высшая квалификационная категория
        </td>
        <td itemProp="teachingDiscipline">Теория и устройство судна</td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">Инженер кораблестроитель</td>
        <td itemProp="employeeQualification">Судостроение и судоремонт</td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          "Педагогика и психология в системе СПО" (2021); Стажировка: изучил
          специфику работ по обработке и размещению груза,управлению
          барже-буксирным составом,использованию технических средств
          судовождения,конструкцию корпуса, судовые вспомогательные механизмы и
          устройства, общую и специальную лоцию р. Белой.( 2021); "Обучение
          работников по оказанию первой помощи пострадавшим"(2023)
        </td>
        <td itemProp="genExperience">50</td>
        <td itemProp="specExperience">28</td>
        <td itemProp="teachingOp">
          23.02.01 "Организация перевозок и управление на транспорте (по видам)"
        </td>
      </tr>{" "}
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Галимова Альмира Фагимовна</td>
        <td itemProp="post">
          Преподаватель, высшая квалификационная категория
        </td>
        <td itemProp="teachingDiscipline">Математика</td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">Учитель математики</td>
        <td itemProp="employeeQualification">Математика</td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          "Преподавание математики в условиях реализации ФГОС" (дистанционно)
          2022; " Разработка, внедерние и сертификация системы менеджмента
          качества организации"(2022);"Обучение работников по оказанию первой
          помощи пострадавшим"(2023)
        </td>
        <td itemProp="genExperience">19</td>
        <td itemProp="specExperience">19</td>
        <td itemProp="teachingOp">
          23.02.01 "Организация перевозок и управление на транспорте (по видам)"
        </td>
      </tr>{" "}
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Галлямова Раушания Харисовна</td>
        <td itemProp="post">
          Преподаватель, высшая квалификационная категория
        </td>
        <td itemProp="teachingDiscipline">Физика</td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">Физик</td>
        <td itemProp="employeeQualification">Физика</td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          "Обучение работников по оказанию первой помощи пострадавшим" (2023)
        </td>
        <td itemProp="genExperience">29</td>
        <td itemProp="specExperience">23</td>
        <td itemProp="teachingOp">
          23.02.01 "Организация перевозок и управление на транспорте (по видам)"
        </td>
      </tr>{" "}
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Жилина Анна Викторовна</td>
        <td itemProp="post">
          Преподаватель, высшая квалификационная категория
        </td>
        <td itemProp="teachingDiscipline">
          Информатика. Информационное обеспечение перевозочного процесса /по
          видам транспорта/. Автоматизированные системы управления на транспорте
          /по видам транспорта/
        </td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">Учитель математики и информатики</td>
        <td itemProp="employeeQualification">Математика и информатика</td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          "Применение современных компьютерных технологий в процессе обучения
          информатики в условиях реализации ФГОС"(2020); "Инновационные подходы
          к организации учебной деятельности и методикам преподавания дисциплины
          "Информатика" в организациях СПО с учетом требований ФГОС СПО"( 2023);
          "Обучение работников по оказанию первой помощи пострадавшим"(2023)
        </td>
        <td itemProp="genExperience">23</td>
        <td itemProp="specExperience">22</td>
        <td itemProp="teachingOp">
          23.02.01 "Организация перевозок и управление на транспорте (по видам)"
        </td>
      </tr>{" "}
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Зкриева Гульнара Робертовна</td>
        <td itemProp="post">Преподаватель высшая квалификационная категория</td>
        <td itemProp="teachingDiscipline">Электротехника и электроника</td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">Инженер-электрик</td>
        <td itemProp="employeeQualification">
          Электрофикация и автоматизация сельского хозяйства
        </td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          "Педагогика и психология в системе СПО" (2021); Стажировка :
          "Электромонтажные работы" (2021); Стажировка: изучила специфику работ
          по электрооборудованию судов: электрических машин, аппаратуры
          управления и защиты, судовых электроприводов и электрических систем
          автоматики и контроля (2021); "Методические подходы к формированию
          образователной программы в рамках переходы на ФГОС-3++" (2021);
          "Обучение работников по оказанию первой помощи пострадавшим"(2023)
        </td>
        <td itemProp="genExperience">29</td>
        <td itemProp="specExperience">28</td>
        <td itemProp="teachingOp">
          23.02.01 "Организация перевозок и управление на транспорте (по видам)"
        </td>
      </tr>{" "}
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Зараев Ильшат Фаритович</td>
        <td itemProp="post">
          Преподаватель, первая квалификационная категория
        </td>
        <td itemProp="teachingDiscipline">Химия</td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">Учитель биологии и химии</td>
        <td itemProp="employeeQualification">Биология и химия</td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          "Современные образовательные технологии в преподавании химии с учетом
          ФГОС" (2020); "Основы предмета "Экология в соответствии с требованиями
          ФГОС" (2020); "Методика преподавания астрономии при подготовке
          специалистов среднего звена" (2020); "Обучение работников по оказанию
          первой помощи пострадавшим"(2023)
        </td>
        <td itemProp="genExperience">16</td>
        <td itemProp="specExperience">16</td>
        <td itemProp="teachingOp">
          23.02.01 "Организация перевозок и управление на транспорте (по видам)"
        </td>
      </tr>{" "}
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Исхакова Лилия Лутфулловна</td>
        <td itemProp="post">
          Преподаватель, первая квалификационная категория
        </td>
        <td itemProp="teachingDiscipline">
          Транспортная система России. Технология перевозочного процесса.
          Обеспечение грузовых перевозок. Экономика и управление на водном
          транспорте. Перевзки в особых условиях. Квалификационный экзамен.
          Технические средства /по видам/. Организация движения
        </td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">Бакалавр</td>
        <td itemProp="employeeQualification">
          Технология транспортных процессов
        </td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          "Педагогика и психология в системе СПО" (2021); Цифровые технологии в
          преподавании профильных дисциплин (2022);"Обучение работников по
          оказанию первой помощи пострадавшим"(2023)
        </td>
        <td itemProp="genExperience">22</td>
        <td itemProp="specExperience">5</td>
        <td itemProp="teachingOp">
          23.02.01 "Организация перевозок и управление на транспорте (по видам)"
        </td>
      </tr>{" "}
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Козырь Снежана Андреевна</td>
        <td itemProp="post">
          Преподаватель, высшая квалификационная категория
        </td>
        <td itemProp="teachingDiscipline">Русский язык. Литература</td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">Бакалавр</td>
        <td itemProp="employeeQualification">Филология</td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          "Особенности деятельности педагогических работников СПО в свете
          требований профессионального стандарта"(2020); "Исследовательская
          технология на уроках русского языка и литературы по ФГОС"(2020);
          "Скрайбинг и веб-инвест как инновационные образовательные технологии в
          условиях реализации ФГОС СПО"(2022);"Обучение работников по оказанию
          первой помощи пострадавшим"(2023)
        </td>
        <td itemProp="genExperience">6</td>
        <td itemProp="specExperience">6</td>
        <td itemProp="teachingOp">
          23.02.01 "Организация перевозок и управление на транспорте (по видам)"
        </td>
      </tr>{" "}
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Кульмухаметова Гульнара Айдаровна</td>
        <td itemProp="post">Преподаватель</td>
        <td itemProp="teachingDiscipline">Инженерная графика</td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">Магистр</td>
        <td itemProp="employeeQualification">
          Технологические машины и обрудования
        </td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          "Методические подходы к формированию образовательной программы в
          рамках перехода на ФГОС-3++"(2021); Цифровые технологии в преподавании
          профильных дисциплин" (2022);"Обучение работников по оказанию первой
          помощи пострадавшим"(2023)
        </td>
        <td itemProp="genExperience">10</td>
        <td itemProp="specExperience">9</td>
        <td itemProp="teachingOp">
          23.02.01 "Организация перевозок и управление на транспорте (по видам)"
        </td>
      </tr>{" "}
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Муратов Раис Фаритович</td>
        <td itemProp="post">Преподаватель</td>
        <td itemProp="teachingDiscipline">
          Технические средства судовождения. Правовое обеспечение. Технология
          перевозочного процесса. Квалификационный экзамен
        </td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">Инженер водного транспорта</td>
        <td itemProp="employeeQualification">
          Эксплуатация водного транспорта
        </td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          "Методические подходы к формированию образовательной программы в
          рамках перехода на ФГОС-3++"(2021); "Обучение работников по оказанию
          первой помощи пострадавшим"(2023)
        </td>
        <td itemProp="genExperience">43</td>
        <td itemProp="specExperience">12</td>
        <td itemProp="teachingOp">
          23.02.01 "Организация перевозок и управление на транспорте (по видам)"
        </td>
      </tr>{" "}
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Миннулина Светлана Рафисовна</td>
        <td itemProp="post">
          Заведующий отделением СПО, преподаватель высшей категории
        </td>
        <td itemProp="teachingDiscipline">Физика. Индувидуальный проект</td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">Учитель информатики</td>
        <td itemProp="employeeQualification">Информатика</td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          "Обучение работников по оказанию первой помощи пострадавшим" (2023)
        </td>
        <td itemProp="genExperience">15</td>
        <td itemProp="specExperience">15</td>
        <td itemProp="teachingOp">
          23.02.01 "Организация перевозок и управление на транспорте (по видам)"
        </td>
      </tr>{" "}
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Мухутдинова Лилия Ирековна</td>
        <td itemProp="post">
          Преподаватель, высшая квалификационная категория
        </td>
        <td itemProp="teachingDiscipline">
          Иностранный язык в профессиональной деятельности
        </td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">Филолог. Преподаватель немецкого языка</td>
        <td itemProp="employeeQualification">Немецкий язык. Филология</td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          {" "}
          "Особенности деятельности педагогических работников СПО в свете
          требований профессионального стандарта"(2020); "Специфика преподавания
          английского языка с учетом требований ФГОС"(2018); "Дизайн
          претентаций"(2022);"Обучение работников по оказанию первой помощи
          пострадавшим"(2023)
        </td>
        <td itemProp="genExperience">29</td>
        <td itemProp="specExperience">29</td>
        <td itemProp="teachingOp">
          23.02.01 "Организация перевозок и управление на транспорте (по видам)"
        </td>
      </tr>{" "}
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Рябова Зинаида Николаевна</td>
        <td itemProp="post">Преподаватель</td>
        <td itemProp="teachingDiscipline">Физическая культура</td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">
          Специалист по физической культуре и спорту
        </td>
        <td itemProp="employeeQualification">Физическая культура и спорт</td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          "Обучение работников по оказанию первой помощи пострадавшим"(2023)
        </td>
        <td itemProp="genExperience">18</td>
        <td itemProp="specExperience">18</td>
        <td itemProp="teachingOp">
          23.02.01 "Организация перевозок и управление на транспорте (по видам)"
        </td>
      </tr>{" "}
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Решетников Виталий Евгеньевич</td>
        <td itemProp="post">Преподаватель</td>
        <td itemProp="teachingDiscipline">
          История. Обществознание. Основы философии
        </td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">Историк. Преподаватель истории</td>
        <td itemProp="employeeQualification">История</td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          {" "}
          "Обучение работников по оказанию первой помощи пострадавшим"(2023)
        </td>
        <td itemProp="genExperience">21</td>
        <td itemProp="specExperience">13</td>
        <td itemProp="teachingOp">
          23.02.01 "Организация перевозок и управление на транспорте (по видам)"
        </td>
      </tr>{" "}
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Филлипова Снежана Васильевна</td>
        <td itemProp="post">Преподаватель</td>
        <td itemProp="teachingDiscipline">
          Транспортная система России. Правовое обеспечение. Основы
          делопроизводства. Физическая культура. ОБЖ
        </td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">
          Инженер транспортных и технологических машин и оборудования
          /нефтегазодобыча/
        </td>
        <td itemProp="employeeQualification">
          Сервис транспортных и технологических машин и оборудования
        </td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          Переподготовка:"Физкультурно-оздоровительная и спортивно-массовая
          работа с наседением"(2020);"Обучение работников по оказанию первой
          помощи" пострадавшим"(2023)
        </td>
        <td itemProp="genExperience">14</td>
        <td itemProp="specExperience">16</td>
        <td itemProp="teachingOp">
          23.02.01 "Организация перевозок и управление на транспорте (по видам)"
        </td>
      </tr>{" "}
      <tr itemProp="teachingStaff">
        <td itemProp="fio">Романов Зульфир Вахитович</td>
        <td itemProp="post">
          Преподаватель, высшая квалификационная категория
        </td>
        <td itemProp="teachingDiscipline">
          ОБЖ Безопасность жизнедеятельности
        </td>
        <td itemProp="teachingLevel">Высшее образование</td>
        <td itemProp="teachingQual">Историк. Преподаватель истории</td>
        <td itemProp="employeeQualification">История</td>
        <td itemProp="degree">-</td>
        <td itemProp="academStat">-</td>
        <td itemProp="profDevelopment">
          Профессиональная переподготовка : Гражданская оборона и защита
          населения от чрезвычайных ситуаций для должностных лиц"(2021)
        </td>
        <td itemProp="genExperience">13</td>
        <td itemProp="specExperience">8</td>
        <td itemProp="teachingOp">
          23.02.01 "Организация перевозок и управление на транспорте (по видам)"
        </td>
      </tr>
    </>
  );
};
export default InfoAboutPrepodPerevozki;
