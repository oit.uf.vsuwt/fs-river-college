import Budget from "./components/page/sveden/budget/Budget";
import Common from "./components/page/sveden/common/Common";
import Document from "./components/page/sveden/document/Document";
import Education from "./components/page/sveden/education/Education";
import EduStandarts from "./components/page/sveden/eduStandarts/EduStandarts";
import Employees from "./components/page/sveden/employees/Employees";
import Grants from "./components/page/sveden/grants/Grants";
import Inter from "./components/page/sveden/inter/Inter";
import Ovz from "./components/page/sveden/ovz/Ovz";
import Objects from "./components/page/sveden/objects/Objects";
import PaidEdu from "./components/page/sveden/paidEdu/PaidEdu";
import Struct from "./components/page/sveden/struct/Struct";
import Vacant from "./components/page/sveden/vacant/Vacant";
import Main from "./layouts/main";
import Sveden from "./layouts/sveden/Sveden";
import Live from "./layouts/live";
import StudentsLive from "./components/page/live/studentsLive/StudentsLive";
import Sport from "./components/page/live/sport/Sport";
import OurPride from "./components/page/live/ourPride/OurPride";
import Science from "./components/page/live/science/Science";
import Learning from "./layouts/Learning";
import Practice from "./components/page/learning/Practice/Practice";
import EmploymentService from "./components/page/learning/EmploymentService/EmploymentService";
import About from "./components/page/learning/EmploymentService/about/About";
import Faq from "./components/page/learning/EmploymentService/faq/Faq";
import Partners from "./components/page/learning/EmploymentService/partners/Partners";
import VacanciesFrom from "./components/page/learning/EmploymentService/vacanciesFrom/VacanciesFrom";
import Enrolly from "./layouts/Enrollee";
import OpenDay from "./components/page/enrollee/openDay/OpenDay";
import ApplicantsInfo from "./components/page/enrollee/applicants/applicantsInfo/ApplicantsInfo";
import Message from "./components/page/enrollee/applicants/message/Message";
import Monitoring from "./components/page/enrollee/applicants/monitoring/Monitoring";
import Reception from "./components/page/enrollee/applicants/reception/Reception";
import SubmissionDoc from "./components/page/enrollee/applicants/submissionDoc/SubmissionDoc";
import EducationCosts from "./components/page/enrollee/applicants/reception/educationCosts/EducationCosts";
import ReceptionPlaces from "./components/page/enrollee/applicants/reception/receptionPlaces/ReceptionPlaces";
import ReceptionTiming from "./components/page/enrollee/applicants/reception/receptionTiming/ReceptionTiming";
import Training from "./layouts/Training";
import AboutCdo from "./components/page/training/aboutCdo/AboutCdo";
import AdvancedTraining from "./components/page/training/advancedTraining/AdvancedTraining";
import DistanceLearning from "./components/page/training/distanceLearning/DistanceLearning";
import Home from "./layouts/Home";
import AboutBranch from "./components/page/homePage/aboutBranch/AboutBranch";
import HistoricalReference from "./components/page/homePage/historicalReference/HistoricalReference";
import UniversityNews from "./components/page/homePage/universityNews/UniversityNews";
import UniversityEvents from "./components/page/homePage/universityEvents/UniversityEvents";
import ListsOfPersonsWhoAppliedForAdmission from "./components/page/enrollee/applicants/monitoring/listsOfPersonsWhoAppliedForAdmission/ListsOfPersonsWhoAppliedForAdmission";
import CompetitionLists from "./components/page/enrollee/applicants/monitoring/competitionLists/СompetitionLists";
import EnlistedOrders from "./components/page/enrollee/applicants/monitoring/enlistedOrders/EnlistedOrders";
import DraftOrdersForEnrollment from "./components/page/enrollee/applicants/monitoring/draftOrdersForEnrollment/DraftOrdersForEnrollment";
import EducationPrograms from "./layouts/EducationPrograms";
import OrganizationOfTransportation from "./components/page/educationsPrograms/230201/OrganizationOfTransportation";
import OrganizationOfTransportation2020 from "./components/page/educationsPrograms/230201/OrganizationOfTransportation2020";
import Navigation2019 from "./components/page/educationsPrograms/260203/Navigation2019";
import Navigation2020 from "./components/page/educationsPrograms/260203/Navigation2020";
import NavigationDepth from "./components/page/educationsPrograms/260203/NavigationDepth";
import ShipPowerPlants2020 from "./components/page/educationsPrograms/260205/ShipPowerPlants2020";
import MarineElectricalEquipment2020 from "./components/page/educationsPrograms/260206/MarineElectricalEquipment2020";
import EducationLvlRequirements from "./components/page/enrollee/applicants/reception/educationLvlRequirements/EducationLvlRequirements";
import AdmissionConditions from "./components/page/enrollee/applicants/reception/admissionConditions/AdmissionConditions";
import ProfessionalTrainingPrograms from "./components/page/training/professionalTrainingPrograms/ProfessionalTrainingPrograms";
import ShipPowerPlantsVO2023 from "./components/page/educationsPrograms/260506/ShipPowerPlantsVO2023";
import ShipPowerPlantsVO2023FOS from "./components/page/educationsPrograms/260506/ShipPowerPlantsVO2023FOS";
import KCP from "./components/page/enrollee/applicants/reception/KCP/KCP";

import Contacts from "./layouts/contacts";
import ContactsPage from "./components/page/contacts/contacts/contacts";
import Reviews from "./components/page/contacts/reviews/reviews";
import Vacancies from "./components/page/contacts/vacancies/Vacancies";

import AddEducationProgramsChildrenAdults from "./components/page/training/add-education-programs-children-adults/add-education-programs-children-adults";
import Cadets from "./components/page/learning/cadets/cadets";
import Parents from "./components/page/learning/parents/parents";
import CorrespondenceStudents from "./components/page/learning/correspondence-students/correspondence-students";
import Dormitory from "./components/page/enrollee/applicants/dormitory/dormitory";
import EducationsPlatforms from "./components/page/learning/educations-platforms";
import AutoSchedule from "./components/page/schedule/auto-schedule";
import LocalActs from "./components/page/sveden/education/local-acts";
import Navigation2020FOS from "./components/page/educationsPrograms/260203/Navigation2020FOS";
import Navigation2019FOS from "./components/page/educationsPrograms/260203/Navigation2019FOS";
import Navigation2019FOSZ from "./components/page/educationsPrograms/260203/Navigation2019FOS-Z";
import MarineElectricalEquipment2020FOS from "./components/page/educationsPrograms/260206/MarineElectricalEquipment2020FOS";
import ShipPowerPlants2020FOS from "./components/page/educationsPrograms/260205/ShipPowerPlants2020FOS";
import OrganizationOfTransportation2020FOS from "./components/page/educationsPrograms/230201/OrganizationOfTransportation2020FOS";
import Navigation2021 from "./components/page/educationsPrograms/260203/Navigation2021";
import Navigation2021FOS from "./components/page/educationsPrograms/260203/Navigation2021FOS";
import MarineElectricalEquipment2021 from "./components/page/educationsPrograms/260206/MarineElectricalEquipment2021";
import MarineElectricalEquipment2021FOS from "./components/page/educationsPrograms/260206/MarineElectricalEquipment2021FOS";
import ShipPowerPlants2021 from "./components/page/educationsPrograms/260205/ShipPowerPlants2021";
import ShipPowerPlants2021FOS from "./components/page/educationsPrograms/260205/ShipPowerPlants2021FOS";
import OrganizationOfTransportation2021 from "./components/page/educationsPrograms/230201/OrganizationOfTransportation2021";
import OrganizationOfTransportation2021FOS from "./components/page/educationsPrograms/230201/OrganizationOfTransportation2021FOS";
import Navigation2022 from "./components/page/educationsPrograms/260203/Navigation2022";
import Navigation2022FOS from "./components/page/educationsPrograms/260203/Navigation2022FOS";
import ShipPowerPlants2022FOS from "./components/page/educationsPrograms/260205/ShipPowerPlants2022FOS";
import ShipPowerPlants2022 from "./components/page/educationsPrograms/260205/ShipPowerPlants2022";
import MarineElectricalEquipment2022 from "./components/page/educationsPrograms/260206/MarineElectricalEquipment2022";
import MarineElectricalEquipment2022FOS from "./components/page/educationsPrograms/260206/MarineElectricalEquipment2022FOS";
import OrganizationOfTransportation2022 from "./components/page/educationsPrograms/230201/OrganizationOfTransportation2022";
import OrganizationOfTransportation2022FOS from "./components/page/educationsPrograms/230201/OrganizationOfTransportation2022FOS";
import Spo from "./components/page/enrollee/applicants/reception/spo/spo";
import HigherEducation from "./components/page/enrollee/applicants/reception/vo/vo";
import PassingScores from "./components/page/enrollee/applicants/reception/passing-scores/passing-scores";
import KcpVo from "./components/page/enrollee/applicants/reception/KCP-VO/kcp-vo";
import Schedule from "./layouts/schedule";
import AutoScheduleVo from "./components/page/schedule/auto-shedule-vo";
import StateForPeople from "./components/page/homePage/state-for-people/state-for-people";
import OneNewsPage from "./components/page/one-news-page/one-news-page";
import RequireAuth from "./hoc/require-auth";
import Auth from "./layouts/auth/auth";
import AdminPanel from "./components/page/admin-panel/admin-panel";
import EditorNewsPage from "./components/page/create-news-page/create-news-page";
import EditNewsPage from "./components/page/create-news-page/edit-news-page";
import Interview from "./components/page/live/interview/interview";
import Abkadirov from "./components/page/live/interview/abkadirov";
import Portnyagina from "./components/page/live/interview/portnyagina";
import Timofeev from "./components/page/live/interview/timofeev";
import Tokmakov from "./components/page/live/interview/tokmakov";
const routes = () => [
  {
    path: "",
    element: <Main />,
  },
  {
    path: ":id",
    element: <OneNewsPage />,
  },
  /*Авторизированные пользователи*/
  {
    path: "auth",
    element: (
      <RequireAuth>
        <Auth />
      </RequireAuth>
    ),
  },
  {
    path: "admin",
    element: (
      <RequireAuth>
        <AdminPanel />
      </RequireAuth>
    ),
    children: [
      {
        path: "editorNews",
        element: (
          <RequireAuth>
            <EditorNewsPage />
          </RequireAuth>
        ),
      },
    ],
  },
  {
    path: "editNewsPage/:id",
    element: (
      <RequireAuth>
        <EditNewsPage />
      </RequireAuth>
    ),
  },
  {
    path: "educationPrograms",
    element: <EducationPrograms />,
    children: [
      {
        path: "organizationOfTransportation",
        element: <OrganizationOfTransportation />,
      },
      {
        path: "organizationOfTransportation2020",
        element: <OrganizationOfTransportation2020 />,
      },
      {
        path: "organizationOfTransportation2020FOS",
        element: <OrganizationOfTransportation2020FOS />,
      },
      {
        path: "organizationOfTransportation2021",
        element: <OrganizationOfTransportation2021 />,
      },
      {
        path: "organizationOfTransportation2021FOS",
        element: <OrganizationOfTransportation2021FOS />,
      },
      {
        path: "organizationOfTransportation2022",
        element: <OrganizationOfTransportation2022 />,
      },
      {
        path: "organizationOfTransportation2022FOS",
        element: <OrganizationOfTransportation2022FOS />,
      },
      {
        path: "navigation2019",
        element: <Navigation2019 />,
      },

      { path: "navigation2019FOS", element: <Navigation2019FOS /> },
      { path: "navigation2019FOSZ", element: <Navigation2019FOSZ /> },
      {
        path: "navigation2020",
        element: <Navigation2020 />,
      },
      { path: "navigation2020FOS", element: <Navigation2020FOS /> },
      {
        path: "navigation2021",
        element: <Navigation2021 />,
      },
      { path: "navigation2022FOS", element: <Navigation2021FOS /> },
      {
        path: "navigation2022",
        element: <Navigation2022 />,
      },
      { path: "navigation2022FOS", element: <Navigation2022FOS /> },
      {
        path: "navigationDepth",
        element: <NavigationDepth />,
      },
      {
        path: "shipPowerPlants2020",
        element: <ShipPowerPlants2020 />,
      },
      {
        path: "shipPowerPlants2020FOS",
        element: <ShipPowerPlants2020FOS />,
      },
      {
        path: "shipPowerPlants2021",
        element: <ShipPowerPlants2021 />,
      },
      {
        path: "shipPowerPlants2021FOS",
        element: <ShipPowerPlants2021FOS />,
      },
      {
        path: "shipPowerPlants2022",
        element: <ShipPowerPlants2022 />,
      },
      {
        path: "shipPowerPlants2022FOS",
        element: <ShipPowerPlants2022FOS />,
      },
      {
        path: "shipPowerPlantsVO2023",
        element: <ShipPowerPlantsVO2023 />,
      },
      {
        path: "shipPowerPlantsVO2023FOS",
        element: <ShipPowerPlantsVO2023FOS />,
      },
      {
        path: "marineElectricalEquipment2020",
        element: <MarineElectricalEquipment2020 />,
      },
      {
        path: "marineElectricalEquipment2020FOS",
        element: <MarineElectricalEquipment2020FOS />,
      },
      {
        path: "marineElectricalEquipment2021",
        element: <MarineElectricalEquipment2021 />,
      },
      {
        path: "marineElectricalEquipment2021FOS",
        element: <MarineElectricalEquipment2021FOS />,
      },
      {
        path: "marineElectricalEquipment2022",
        element: <MarineElectricalEquipment2022 />,
      },
      {
        path: "marineElectricalEquipment2022FOS",
        element: <MarineElectricalEquipment2022FOS />,
      },
    ],
  },
  {
    path: "local-acts",
    element: <LocalActs />,
  },
  {
    path: "schedule",
    element: <Schedule />,
    children: [
      {
        path: "",
        element: <AutoSchedule />,
      },
      {
        path: "vo",
        element: <AutoScheduleVo />,
      },
    ],
  },
  {
    path: "sveden",
    element: <Sveden />,
    children: [
      {
        path: "common",
        element: <Common />,
      },
      {
        path: "budget",
        element: <Budget />,
      },
      {
        path: "document",
        element: <Document />,
      },
      {
        path: "education",
        element: <Education />,
      },
      {
        path: "eduStandarts",
        element: <EduStandarts />,
      },
      {
        path: "employees",
        element: <Employees />,
      },
      {
        path: "grants",
        element: <Grants />,
      },
      {
        path: "inter",
        element: <Inter />,
      },
      {
        path: "objects",
        element: <Objects />,
      },
      {
        path: "ovz",
        element: <Ovz />,
      },
      {
        path: "paid__edu",
        element: <PaidEdu />,
      },
      {
        path: "struct",
        element: <Struct />,
      },
      {
        path: "vacant",
        element: <Vacant />,
      },
    ],
  },
  {
    path: "live",
    element: <Live />,
    children: [
      {
        path: "interview",
        element: <Interview />,
      },
      {
        path: "timofeev",
        element: <Timofeev />,
      },
      {
        path: "tokmakov",
        element: <Tokmakov />,
      },
      {
        path: "abkadirov",
        element: <Abkadirov />,
      },
      {
        path: "portnyagina",
        element: <Portnyagina />,
      },
      {
        path: "studentsLive",
        element: <StudentsLive />,
      },
      {
        path: "studentsLive/:id",
        element: <OneNewsPage />,
      },
      {
        path: "sport",
        element: <Sport />,
      },
      {
        path: "sport/:id",
        element: <OneNewsPage />,
      },
      {
        path: "ourPride",
        element: <OurPride />,
      },
      {
        path: "ourPride/:id",
        element: <OneNewsPage />,
      },
      {
        path: "science",
        element: <Science />,
      },
      {
        path: "science/:id",
        element: <OneNewsPage />,
      },
    ],
  },
  {
    path: "enrollee",
    element: <Enrolly />,
    children: [
      {
        path: "kcp",
        element: <KCP />,
      },
      {
        path: "kcpVo",
        element: <KcpVo />,
      },
      /* Скорее всего этот компонент больше будет не нужен, можно будет удалить */
      /*     {
        path: "applicants",
        element: <ApplicantsEnrollee />,
      }, */
      {
        path: "dormitory",
        element: <Dormitory />,
      },
      {
        path: "messageFromDirector",
        element: <Message />,
      },
      {
        path: "applicantsInfo",
        element: <ApplicantsInfo />,
      },
      {
        path: "monitoring",
        element: <Monitoring />,
        children: [
          {
            path: "listsOfPersonsWhoAppliedForAdmission",
            element: <ListsOfPersonsWhoAppliedForAdmission />,
          },
          {
            path: "competitionLists",
            element: <CompetitionLists />,
          },
          {
            path: "draftOrdersForEnrollment",
            element: <DraftOrdersForEnrollment />,
          },
          {
            path: "enlistedOrders",
            element: <EnlistedOrders />,
          },
        ],
      },

      {
        path: "reception",
        element: <Reception />,
      },
      {
        path: "spo",
        element: <Spo />,
      },
      {
        path: "passingScores",
        element: <PassingScores />,
      },
      {
        path: "higherEducation",
        element: <HigherEducation />,
      },
      {
        path: "submissionDoc",
        element: <SubmissionDoc />,
      },
      {
        path: "educationCosts",
        element: <EducationCosts />,
      },
      {
        path: "receptionPlaces",
        element: <ReceptionPlaces />,
      },
      {
        path: "receptionTiming",
        element: <ReceptionTiming />,
      },
      {
        path: "open-day",
        element: <OpenDay />,
      },
      {
        path: "educationLvlRequirements",
        element: <EducationLvlRequirements />,
      },
      {
        path: "admissionConditions",
        element: <AdmissionConditions />,
      },
    ],
  },
  {
    path: "learning",
    element: <Learning />,
    children: [
      {
        path: "practice",
        element: <Practice />,
      },
      {
        path: "educationsPlatforms",
        element: <EducationsPlatforms />,
      },
      {
        path: "cadets",
        element: <Cadets />,
      },
      {
        path: "parents",
        element: <Parents />,
      },
      {
        path: "correspondenceStudents",
        element: <CorrespondenceStudents />,
      },

      {
        path: "employmentService",
        element: <EmploymentService />,
        children: [
          {
            path: "",
            element: <About />,
          },
          {
            path: "faq",
            element: <Faq />,
          },
          {
            path: "partners",
            element: <Partners />,
          },
          {
            path: "vacanciesFrom",
            element: <VacanciesFrom />,
          },
        ],
      },
    ],
  },
  {
    path: "training",
    element: <Training />,
    children: [
      {
        path: "aboutCdo",
        element: <AboutCdo />,
      },
      {
        path: "advancedTraining",
        element: <AdvancedTraining />,
      },
      {
        path: "distanceLearning",
        element: <DistanceLearning />,
      },
      {
        path: "professionalTrainingPrograms",
        element: <ProfessionalTrainingPrograms />,
      },
      {
        path: "addEducationProgramsChildrenAdults",
        element: <AddEducationProgramsChildrenAdults />,
      },
    ],
  },
  {
    path: "contacts",
    element: <Contacts />,
    children: [
      {
        path: "contactsPage",
        element: <ContactsPage />,
      },
      {
        path: "reviews",
        element: <Reviews />,
      },
      {
        path: "vacancies",
        element: <Vacancies />,
      },
    ],
  },
  {
    path: "home",
    element: <Home />,
    children: [
      {
        path: "aboutBranch",
        element: <AboutBranch />,
      },
      {
        path: "stateForPeople",
        element: <StateForPeople />,
      },
      {
        path: "historicalReference",
        element: <HistoricalReference />,
      },
      {
        path: "univercityNews",
        element: <UniversityNews />,
      },
      {
        path: "univercityEvents",
        element: <UniversityEvents />,
      },
    ],
  },
];
export default routes;
